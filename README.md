# APG-Homework

This repositary contains semester projects.

## Homework 0+1

### Data structures

 - Context manager
   - holds information about contexts, loads contexts
   - map<id,context>
   - active context
   <br/>

 - Context
   - <strike>id</strike> not needed, if manager contains this information
   - width, height
   - colorbuffer
   - vertexbuffer
   - matrix stack? -- array with pop, push, access current top, 2x ortho/prespective
   - *current matrix? (top of stack)
   - bool Drawing() (1 between sglBegin and sglEnd calls)
   - element to draw
   - point size
   - current color
   <br/>

 - Matrix
   - 3x3 seems sufficient for first homework, will definitely need 4x4 later
   - operations:
     - matrix * matrix
     - matrix * vector
     - matrix +- matrix
     - scalar * matrix?
     - function to make identity matrix
   <br/>

 - Vector
   - size 2, size 3, size 4
   - maybe only size 4 where rest will be z=0, w=1 (size 2); w = 1 (size 3) ??  
   - operations:
     - dot product (not needed yet?)
     - cross product (not needed yet?)
     - vec +- vec
     - scalar * vec?

### Needed sgl functions

 - [x] sglSetContext
 - [x] sglGetContext
 - [x] sglCreateContext
 - [x] sglDestroyContext
 - [ ] sglInit
 - [x] sglFinish<br/><br/>
 
 - [x] sglClearColor
 - [x] sglClear
 - [x] sglViewport
 - [x] sglDisable
 - [x] sglAreaMode
 - [x] sglBegin
 - [x] sglEnd
 - [x] sglVertex2f 
 - [x] sglVertex3f 
 - [x] sglVertex4f 
 - [x] sglGetColorBufferPointer<br/><br/>

 - [x] sglMatrixMode
 - [x] sglPushMatrix
 - [x] sglPopMatrix
 - [x] sglLoadIdentity
 - [x] sglOrtho
 - [x] sglTranslate
 - [x] sglRotate2D
 - [x] sglScale<br/><br/>

 - [x] sglColor3f
 - [x] sglPointSize<br/><br/>

 - [x] sglCircle
 - [x] sglEllipse
 - [x] sglArc<br/><br/>

 YEP IT'S CODING TIME :frog: :gun:

## Homework 2 - Fill it!
 - Non-convex polygon filling
 - Fill circle/ellipse/arc
 - perspective projection (sglFrustrum)
 - Depth management -> z-buffer
### Functions
 - [x] sglFurstrum()<br/><br/>

 - [x] Context::UpdatePVM() just an upgrade of matrix updating function<br/><br/>

 - [x] Context::CalcDepth() linear interpolation function<br/><br/>

 - [x] Context::DrawPointDepth()
 - [x] Context::DrawLineDepth()
 - [x] Context::DrawArc()<br/><br/>

 - [x] Context::Fill()

 - [x] BH:: binary heap for edge processing
 - [x] Edge:: edges of an object

Round two. Fight!!!