#include "BH.h"
#include "MatrixVector.h"
#include <vector>
#include <iostream>
#include <cmath>

Edge::Edge() {
    yMin = 0;
    yMax = 0;
    x = 0;
    slope = 0.0f;
}

Edge::Edge(const Vec4<float> & v1, const Vec4<float> & v2) {
    if (v1[1] < v2[1]) {
        yMin = v1[1];
        yMax = v2[1];
        x = v1[0];
        xMax = v2[0];
        zMin = v1[2];
        zMax = v2[2];
    }
    else {
        yMin = v2[1];
        yMax = v1[1];
        x = v2[0];
        xMax = v1[0];
        zMin = v2[2];
        zMax = v1[2];
    }
    slope = 1.f / ((float)(v2[1] - v1[1]) / (float)(v2[0] - v1[0]));
}

//bool Edge::operator<(const Edge& e2) {
//    return (x < e2.x);
//}

bool BinaryHeap::CompareIdxs(int parent, int child) {
    if (edges[parent].yMin > edges[child].yMin)
        return true;
    return false;
}

bool BinaryHeap::Empty() {
    return max == 0;
}

void BinaryHeap::Add(Edge key) {
    if (edges.empty() || (int)edges.size() == max)
        edges.push_back(key);
    else if ((int)edges.size() > max) {
        edges[max++] = key;
    }
    int ch = max, par = floor((ch - 1) / 2);
    max++;

    while (ch != 0 && CompareIdxs(par, ch)) {
        swapBin(par, ch);
        ch = par;
        par = floor((ch - 1) / 2);
    }
}

void BinaryHeap::MinHeapify(int i) {
    int left = 2 * i + 1, right = left + 1, small = i;
    if (left < max && CompareIdxs(small, left))
        small = left;
    if (right < max && CompareIdxs(small, right))
        small = right;
    if (small != i) {
        swapBin(i, small);
        return MinHeapify(small);
    }
}

Edge BinaryHeap::extractMin() {
    Edge minRoad;
    if (max == 0) {
        return Edge();
    }
    minRoad = edges[0];
    edges[0] = edges[max - 1];
    max--;
    MinHeapify(0);
    return minRoad;
}

void BinaryHeap::swapBin(const int& par, const int& ch) {
    Edge tmp = edges[ch];
    edges[ch] = edges[par];
    edges[par] = tmp;
}

void BinaryHeap::PrintQueue() {
    for (int i = 0; i < max; i++)
        std::cout << edges[i].yMin << " to " << edges[i].yMax << " || " << edges[i].x << std::endl;
}