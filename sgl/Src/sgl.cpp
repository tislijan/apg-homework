//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2016/10/24
// Author: Jaroslav Sloup
// Author: Jaroslav Krivanek, Jiri Bittner, CTU Prague
// Edited: Jakub Hendrich, Daniel Meister, CTU Prague
//---------------------------------------------------------------------------

#include <map>
#include <stack>
#include <algorithm>
#include <cmath>
#include <thread>
#include "sgl.h"
#include "ContextBuffers.h"
#include "MatrixVector.h"
#include "SceneRes.h"


using namespace std;

/// Current error code.
static sglEErrorCode _libStatus = SGL_NO_ERROR;

static inline void setErrCode(sglEErrorCode c) 
{
  if (_libStatus == SGL_NO_ERROR)
    _libStatus = c;
}

//---------------------------------------------------------------------------
// Context managing variables and functions
//---------------------------------------------------------------------------

// BONUS tasks - SS, different lighting model are bounded to defines in ContextBuffers.h

static int contextID = 0;
static int activeContextID = 0;
static bool definingScene = false;
static Context* activeContext = nullptr;
static map<int, Context*> contextsList;

bool CheckContext() {
    if (activeContext == nullptr) {
        setErrCode(SGL_INVALID_OPERATION);
        return false;
    }
    return true;
}

bool CheckValue(float valueToCheck, float min, float max) {
    if (valueToCheck >= min)
        if (valueToCheck <= max)
            return true;
    return false;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------
sglEErrorCode sglGetError(void) 
{
  sglEErrorCode ret = _libStatus;
  _libStatus = SGL_NO_ERROR;
  return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------
const char* sglGetErrorString(sglEErrorCode error)
{
  static const char *errStrigTable[] = 
  {
    "Operation succeeded",
    "Invalid argument(s) to a call",
    "Invalid enumeration argument(s) to a call",
    "Invalid call",
    "Quota of internal resources exceeded",
    "Internal library error",
    "Matrix stack overflow",
    "Matrix stack underflow",
    "Insufficient memory to finish the requested operation"
  };

  if ((int)error < (int)SGL_NO_ERROR || (int)error > (int)SGL_OUT_OF_MEMORY)
    return "Invalid value passed to sglGetErrorString()"; 

  return errStrigTable[(int)error];
}

//---------------------------------------------------------------------------
// Initialization functions
//---------------------------------------------------------------------------

void sglInit(void) {}

void sglFinish(void) {
    for (auto it = contextsList.begin(); it != contextsList.end(); it++) {
        delete it->second;
    }
    contextsList.clear();
}

int sglCreateContext(int width, int height) {
    // error handling?
    if (width < 0 || height < 0) {
        setErrCode(SGL_INVALID_VALUE);
        return -1;
    }

    Context * ctx = new Context(width, height);
    contextsList[contextID++] = ctx;
    return (contextID - 1);
}

void sglDestroyContext(int id) {
    map<int, Context*>::iterator ctxIt = contextsList.find(id);
    if (ctxIt == contextsList.end())
        setErrCode(SGL_INVALID_VALUE);
    else if (id == activeContextID)
        setErrCode(SGL_INVALID_OPERATION);
    else {
        delete ctxIt->second;
        contextsList.erase(ctxIt);
    }
}

void sglSetContext(int id) {
    map<int, Context*>::iterator ctxIt = contextsList.find(id);
    if (ctxIt == contextsList.end()) {
        setErrCode(SGL_INVALID_VALUE);
        activeContextID = 0;
        activeContext = nullptr;
    }
    else {
        activeContextID = ctxIt->first;
        activeContext = ctxIt->second;
    }
}

int sglGetContext(void) {
    if (activeContext == nullptr) {
        setErrCode(SGL_INVALID_OPERATION);
    }
    return activeContextID;
}

float *sglGetColorBufferPointer(void) {
    return activeContext ? reinterpret_cast<float *>(activeContext->colorBuffer.data()) : nullptr;
}

//---------------------------------------------------------------------------
// Drawing functions
//---------------------------------------------------------------------------

void sglClearColor(float r, float g, float b, float alpha) {
    if (CheckContext())
        activeContext->clearColor = RGB(r, g, b);
}

void sglClear(unsigned what) {
    if (CheckContext()) {
        if (what < SGL_COLOR_BUFFER_BIT || what > (SGL_DEPTH_BUFFER_BIT | SGL_COLOR_BUFFER_BIT)) {
            setErrCode(SGL_INVALID_VALUE);
        }
        if (what & SGL_COLOR_BUFFER_BIT) {
            // clear color buffer with clearcolor
            fill(activeContext->colorBuffer.begin(), activeContext->colorBuffer.end(), activeContext->clearColor);
        }
        if (what & SGL_DEPTH_BUFFER_BIT) {
            // clear depth buffer with float
            fill(activeContext->depthBuffer.begin(), activeContext->depthBuffer.end(),numeric_limits<float>::max());
        }
    }
}

void sglBegin(sglEElementType mode) {
    if (CheckContext()) {
        if (mode > 0 || mode < SGL_LAST_ELEMENT_TYPE)
            activeContext->drawingElement = mode;
        else
            setErrCode(SGL_INVALID_ENUM);
    }

    if (activeContext->contextScene.currObj == nullptr) {
        TriangleMesh* mesh = new TriangleMesh();

        if (activeContext->contextScene.useEmissive) {
            mesh->eMat = activeContext->contextScene.currEmissiveMaterial;
            mesh->isEmissive = true;
            activeContext->contextScene.areaLights.push_back(mesh);
        }
        else {
            mesh->mat = activeContext->contextScene.currMaterial;
        }
        activeContext->contextScene.currObj = mesh;
        activeContext->contextScene.elements.push_back(mesh);
    }
}

void sglEnd(void) {
    if (CheckContext() && !activeContext->vertexBuffer.empty()) {
        if (activeContext->contextScene.currObj == nullptr) {
            //should never happen, would mean sglMaterial was called between sglStart and sglEnd
            //or that sglEnd was called before sglStart
            return;
        }
        if (activeContext->vertexBuffer.size() != 3) {
            //input wasn't a triangle, what to do here?
            return;
        }
        activeContext->contextScene.currObj->triangles.push_back(Triangle(
            activeContext->vertexBuffer[0], 
            activeContext->vertexBuffer[1], 
            activeContext->vertexBuffer[2]));

        activeContext->vertexBuffer.clear();
        activeContext->drawingElement = sglEElementType(0);
        activeContext->contextScene.useEmissive = false;

#ifndef NO_RASTERIZATION
        // rasterization part
        activeContext->Render();
#endif // !NO_RASTERIZATION
    }
}

void sglVertex4f(float x, float y, float z, float w) {
    if (activeContext != nullptr)
        activeContext->vertexBuffer.push_back(Vec4<float>(x,y,z,w));
}

void sglVertex3f(float x, float y, float z) {
    sglVertex4f(x, y, z, 1);
}

void sglVertex2f(float x, float y) {
    sglVertex4f(x, y, 0, 1);
}

void sglCircle(float xs, float ys, float zs, float radius) {
    if (!CheckContext()) return;
    if (radius <= 0) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    // calculate circle middle in Viewport coordinates
    Vec4<float> center(xs, ys, zs, 1);

    if (activeContext->PVMchanged) {
        activeContext->UpdateVPPVM();
        activeContext->PVMchanged = false;
    }
    activeContext->TransformVertex(center);

    Mat4<float> VP_P_MV = activeContext->viewMat * activeContext->PVMmat;

    // calculate scale factor
    float scale = (float)sqrt(((double)VP_P_MV(0, 0) * (double)VP_P_MV(1, 1)) - ((double)VP_P_MV(0, 1) * (double)VP_P_MV(1, 0)));
    float rad = radius * scale;

    if (activeContext->drawingMode == SGL_POINT) {
        if (!activeContext->deptTest) activeContext->DrawPoint(center[0], center[1]);
        else activeContext->DrawPointDepth(center[0], center[1], center[2]);
    }
    else if (activeContext->drawingMode == SGL_LINE || activeContext->drawingMode == SGL_FILL) {
        //DrawCircle --> SetSymPixel() function decides whether to draw lines or filling
        activeContext->DrawCircle(center[0], center[1], center[2], rad);
    }
}

void sglEllipse(float x, float y, float z, float a, float b) {
    if (!CheckContext()) return;
    if (a <= 0 || b <= 0) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    
    if (activeContext->drawingMode == SGL_POINT) {
        Vec4<float> center = Vec4<float>(x, y, z, 1);
        if (activeContext->PVMchanged) {
            activeContext->UpdateVPPVM();
            activeContext->PVMchanged = false;
        }
        activeContext->TransformVertex(center);

        if (!activeContext->deptTest) activeContext->DrawPoint(center[0], center[1]);
        else activeContext->DrawPointDepth(center[0], center[1], center[2]);
    }
    else if (activeContext->drawingMode == SGL_LINE || activeContext->drawingMode == SGL_FILL) {
        activeContext->DrawEllipse(x,y,z,a,b);
    }
}

void sglArc(float x, float y, float z, float radius, float from, float to) {
    if (!CheckContext()) return;
    if (radius <= 0 || from > to || from < 0 || to < 0) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    //from = std::min(from, (float)M_PI * 2); //clamp values
    //to = min(to, (float)M_PI * 2); //clamp values

    if (activeContext->drawingMode == SGL_POINT) {
        Vec4<float> center = Vec4<float>(x, y, z, 1);
        if (activeContext->PVMchanged) {
            activeContext->UpdateVPPVM();
            activeContext->PVMchanged = false;
        }
        activeContext->TransformVertex(center);

        if (!activeContext->deptTest) activeContext->DrawPoint(center[0], center[1]);
        else activeContext->DrawPointDepth(center[0], center[1], center[2]);
    }
    else {
        activeContext->DrawArc(x, y, z, radius, from, to);
    }
}

//---------------------------------------------------------------------------
// Transform functions
//---------------------------------------------------------------------------

void sglMatrixMode(sglEMatrixMode mode) {
    if (CheckContext()) {
        if (mode == SGL_MODELVIEW || mode == SGL_PROJECTION)
            activeContext->matrixMode = mode;
        else
            setErrCode(SGL_INVALID_ENUM);
    }
}

void sglPushMatrix(void) {
    if (CheckContext()) {
        stack<Mat4<float> >& ctxStk = activeContext->transformStack[activeContext->matrixMode];
        if (ctxStk.empty())
            ctxStk.push(Mat4<float>());
        else
            ctxStk.push(ctxStk.top());
    }
    //SGL_STACK_OVERFLOW error - how do we check this?
}

void sglPopMatrix(void) {
    if (CheckContext()) {
        stack<Mat4<float> >& ctxStk = activeContext->transformStack[activeContext->matrixMode];
        activeContext->PVMchanged = true;
        if (ctxStk.size() == 1)
            setErrCode(SGL_STACK_OVERFLOW);
        else
            ctxStk.pop();
    }
}

void sglLoadIdentity(void) {
    if (CheckContext()) {
        activeContext->PVMchanged = true;
        if (activeContext->transformStack[activeContext->matrixMode].empty())
            activeContext->transformStack[activeContext->matrixMode].push(Mat4<float>());
        else
            activeContext->transformStack[activeContext->matrixMode].top() = Mat4<float>();
    }
}

void sglLoadMatrix(const float *matrix) {
    if (CheckContext()) {
        activeContext->PVMchanged = true;
        Mat4<float> tmp = Mat4<float>(matrix[0], matrix[1], matrix[2], matrix[3],
                                      matrix[4], matrix[5], matrix[6], matrix[7], 
                                      matrix[8], matrix[9], matrix[10], matrix[11], 
                                      matrix[12], matrix[13], matrix[14], matrix[15] );
        if (activeContext->transformStack[activeContext->matrixMode].empty())
            activeContext->transformStack[activeContext->matrixMode].push(Mat4<float>());
        else
            activeContext->transformStack[activeContext->matrixMode].top() = tmp;
    }
}

void sglMultMatrix(const float *matrix) {
    if (CheckContext()) {
        activeContext->PVMchanged = true;
        Mat4<float> tmp = Mat4<float>(matrix[0], matrix[1], matrix[2], matrix[3],
                                      matrix[4], matrix[5], matrix[6], matrix[7],
                                      matrix[8], matrix[9], matrix[10], matrix[11],
                                      matrix[12], matrix[13], matrix[14], matrix[15]);
        Mat4<float> topstack = activeContext->transformStack[activeContext->matrixMode].top();
        activeContext->transformStack[activeContext->matrixMode].top() = topstack * tmp;
    }
}

void sglTranslate(float x, float y, float z) {
    if (CheckContext()) {
        activeContext->PVMchanged = true;
        float translate[] = { 1,0,0,0,
                              0,1,0,0,
                              0,0,1,0,
                              x,y,z,1 };
        sglMultMatrix(translate);
    }
}

void sglScale(float scalex, float scaley, float scalez) {
    if (CheckContext()) {
        activeContext->PVMchanged = true;
        float scale[] = { scalex, 0,      0,      0,
                          0,      scaley, 0,      0,
                          0,      0,      scalez, 0,
                          0,      0,      0,      1 };
        sglMultMatrix(scale);
    }
}

void sglRotate2D(float angle, float centerx, float centery) {
    if (CheckContext()) {
        activeContext->PVMchanged = true;
        sglTranslate(centerx, centery, 0);
        float rotate2D[] = { cos(angle), sin(angle), 0, 0,
                             -sin(angle), cos(angle),  0, 0,
                             0,          0,           1, 0,
                             0,          0,           0, 1 };
        sglMultMatrix(rotate2D);
        sglTranslate(-centerx, -centery, 0);
    }
}

// https://robotics.stackexchange.com/questions/10702/rotation-matrix-sign-convention-confusion
// RHS counterclockwise rotation -- first column sin negative, third colum sin positive
void sglRotateY(float angle) {
    activeContext->PVMchanged = true;
    if (CheckContext()) {
        float rotateY[] = {cos(angle), 0, sin(angle),  0,
                           0,          1, 0,           0,
                           -sin(angle),0, cos(angle),  0,
                           0,          0, 0,           1 };
        sglMultMatrix(rotateY);
    }
}

void sglOrtho(float left, float right, float bottom, float top, float near, float far) {
    if (CheckContext()) {
        if (left == right || bottom == top || near == far)
            setErrCode(SGL_INVALID_VALUE);
        else // mozna bude potreba zmenit -2/fn na 2/fn, kazdy znaci jiny system, -2/fn znaci levotocivy system, 2/fn pravotocivy
        {
            activeContext->PVMchanged = true;
            float rl = right - left, tb = top - bottom, fn = far - near;
            float ortho[] = {2 / rl,               0,                   0,                  0,
                             0,                    2 / tb,              0,                  0,
                             0,                    0,                   -2 / fn,            0,
                             -(right + left) / rl, -(top + bottom) / tb, -(far + near) / fn, 1};
            sglMultMatrix(ortho);
        }
    }
}

void sglFrustum(float left, float right, float bottom, float top, float near, float far) {
    if (CheckContext()) {
        if (left == right || bottom == top || near == far || near < 0 || far < 0)
            setErrCode(SGL_INVALID_VALUE);
        else
        {
            activeContext->PVMchanged = true;
            float rl = right - left, tb = top - bottom, fn = far - near;
            float perspective[] = { 2 * near / rl,        0,                      0,                      0,
                                    0,                    2 * near / tb,          0,                      0,
                                    (right + left) / rl,  (top + bottom) / tb,    -(far + near) / fn,     -1,
                                    0,                    0,                      -(2 * far * near) / fn, 0};
            sglMultMatrix(perspective);
        }
    }
}

void sglViewport(int x, int y, int width, int height) {
    if (CheckContext()) {
        if (width < 1 || height < 1)
            setErrCode(SGL_INVALID_VALUE);
        else {
            activeContext->PVMchanged = true;
            activeContext->viewMat = Mat4<float>(width / 2, 0,          0, 0,
                                                 0,         height / 2, 0, 0,
                                                 0,         0,          1, 0,
                                                 x+width/2, y+height/2, 0, 1);
        }
    }
}

//---------------------------------------------------------------------------
// Attribute functions
//---------------------------------------------------------------------------

void sglColor3f(float r, float g, float b) {
    if (CheckContext()) 
        activeContext->color = RGB(r,g,b);
}

void sglAreaMode(sglEAreaMode mode) {
    if (mode != SGL_FILL && mode != SGL_LINE && mode != SGL_POINT)
        setErrCode(SGL_INVALID_ENUM);
    else if (CheckContext())
        activeContext->drawingMode = mode;
}

void sglPointSize(float size) {
    if (size < 1)
        setErrCode(SGL_INVALID_VALUE);
    else if (!CheckContext())
        return;

    //activeContext->pointSize = size;
    activeContext->pointRadius = (int)floor((size - 1) / 2);
}

void sglEnable(sglEEnableFlags cap) {
    if (cap != SGL_DEPTH_TEST) {
        setErrCode(SGL_INVALID_ENUM);
    } 
    else if (CheckContext())
        activeContext->deptTest = true;
}

void sglDisable(sglEEnableFlags cap) {
    if (cap != SGL_DEPTH_TEST) {
        setErrCode(SGL_INVALID_ENUM);
    }
    else if (CheckContext())
        activeContext->deptTest = false;
}

//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0)) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    definingScene = true;
}

void sglEndScene() {
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0)) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    definingScene = false;
    
    //printf("Scene definition results:\n Objects (%d)\n", activeContext->contextScene.elements.size());
    //for (unsigned int i = 0; i < activeContext->contextScene.elements.size(); i++) {
    //    activeContext->contextScene.elements[i]->PrintInfo();
    //    
    //    //if (dynamic_cast<TriangleMesh*>(activeContext->contextScene.elements[i])) {
    //    //    activeContext->contextScene.elements[i]->PrintInfo();
    //    //}
    //    //else if (dynamic_cast<Sphere*>(activeContext->contextScene.elements[i])) {
    //    //    activeContext->contextScene.elements[i]->PrintInfo();
    //    //}
    //    
    //}
}

void sglSphere(const float x,
               const float y,
               const float z,
               const float radius) 
{
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0) || !definingScene) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    else if (radius <= 0) {
        setErrCode(SGL_INVALID_VALUE);
        return; // ???
    }

    activeContext->AddSphere(x,y,z, radius);
}

void sglMaterial(const float r,
                 const float g,
                 const float b,
                 const float kd,
                 const float ks,
                 const float shine,
                 const float T,
                 const float ior) 
{
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0)) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    else if (!CheckValue(r, 0, 255) || !CheckValue(g, 0, 255) || !CheckValue(b, 0, 255)) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    } else if (!CheckValue(kd, 0, 1) || !CheckValue(ks, 0, 1)) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    // sglMaterial separates incoming geometry into new object
    activeContext->contextScene.currObj = nullptr;
    Material m;
    m.color = Vec3<float>(r, g, b);
    m.kd = kd;
    m.ks = ks;
    m.shine = shine;
    m.T = T;
    m.ior = ior;
    activeContext->contextScene.currMaterial = m;

    //activeContext->SetMaterial(); // ???
}

void sglEmissiveMaterial(const float r,
                         const float g,
                         const float b,
                         const float c0,
                         const float c1,
                         const float c2)
{
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0)) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    activeContext->contextScene.currObj = nullptr;
    EmissiveMaterial m;
    m.color = Vec3<float>(r, g, b);
    m.attenuation = Vec3<float>(c0, c1, c2);
    activeContext->contextScene.currEmissiveMaterial = m;
    activeContext->contextScene.useEmissive = true;
}

void sglPointLight(const float x,
                   const float y,
                   const float z,
                   const float r,
                   const float g,
                   const float b) 
{
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0)) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    else if (!CheckValue(r, 0, 255) || !CheckValue(g, 0, 255) || !CheckValue(b, 0, 255)) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    activeContext->AddLight(x,y,z,r,g,b);
}

void sglRayTraceScene() {
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0) || definingScene){
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    vector<thread> workers;
    set<pair<int, int>> superSampleSet;
    //Invert(testmat); //ret val 1 => inversed, ret val 0 - singular, unchanged
    Mat4<float> invMV = activeContext->transformStack[SGL_MODELVIEW].top();
    Mat4<float> invP = activeContext->transformStack[SGL_PROJECTION].top();
    Mat4<float> invVP = activeContext->viewMat;
    if (Invert(invVP) == 0 || Invert(invMV) == 0 || Invert(invP) == 0) {
        printf("One of the matrices are singular!\n");
        return;
    }

    Mat4<float> invMVPVP = invMV * invP * invVP;

    Vec4<float> O_ray(0, 0, 0, 1);
    O_ray = invMV * O_ray;
    
    unsigned numberOfThreads;
    // Set correct number of threads 
    numberOfThreads = thread::hardware_concurrency();
    //tmp set num threads to 1
    //numberOfThreads = 1;

    // Check for its value, on an older machines can hardware_concurrency return zero
    if (numberOfThreads <= 0)
        numberOfThreads = 1;
    //cout << "\nNumber of threads is: " << numberOfThreads << "\n" << endl;
    unsigned bufferSize = (unsigned)activeContext->colorBuffer.size();
    unsigned threadSize = bufferSize / numberOfThreads;
    unsigned threadMod = bufferSize % numberOfThreads;

    //for (unsigned i = 0; i < numberOfThreads; i++) {
    //    cout << "Thread " << i << " start at " << i * threadSize << " and ends at " << (i + 1) * threadSize << endl;
    //}
    //cout << "Size of color buffer is " << bufferSize << endl;

    // Start all threads - basic ray casting
    for (unsigned i = 0; i < numberOfThreads; i++) {
        unsigned start = i * threadSize, stop = (i + 1) * threadSize;
        if (i == numberOfThreads - 1 && threadMod != 0)
            stop += threadMod;
        workers.push_back(thread(&Context::RayCast, activeContext, start,stop, ref(invMVPVP), ref(O_ray)));
    }
    // Join all threads
    for (unsigned i = 0; i < numberOfThreads; i++) {
        workers[i].join();
        //cout << "Thread " << i << " has finished" << endl;
    }

#ifdef SUPERSAMPLING // Can be switched on by uncommenting this define in ContextBuffers.h
    workers.clear();
    // Start all threads - super sampling preprocessing
    for (unsigned i = 0; i < numberOfThreads; i++) {
        workers.push_back(thread(&Context::PSS, activeContext, i*threadSize,(i+1)*threadSize,ref(superSampleSet)));
    }
    // Join all threads
    for (unsigned i = 0; i < numberOfThreads; i++) {
        workers[i].join();
    }    
    workers.clear();
    unsigned setSize = ceil((float)superSampleSet.size() / (float)numberOfThreads);

    // Start all threads - super sampling
    for (unsigned i = 0; i < numberOfThreads; i++) {
        workers.push_back(thread(&Context::MakeSuperSample, activeContext, i*setSize,(i+1)*setSize, ref(superSampleSet),ref(invMVPVP),ref(O_ray)));
    }
    // Join all threads
    for (unsigned i = 0; i < numberOfThreads; i++) {
        workers[i].join();
    }
#endif
    // memory cleanup
    for (unsigned int i = 0; i < activeContext->contextScene.elements.size(); i++) {
        delete activeContext->contextScene.elements[i];
    }
}

void sglRasterizeScene() {
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0) || definingScene) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    // ?? 
}

void sglEnvironmentMap(const int width,
                       const int height,
                       float *texels)
{
    if (!CheckContext())
        return;
    else if (activeContext->drawingElement != sglEElementType(0)) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    activeContext->contextScene.SetBGEnv(width, height, texels);
}