#pragma once
#include "sgl.h"
#include "MatrixVector.h"
#include "BH.h"
#include "SceneRes.h"
#include <stack>
#include <vector>
#include <iostream>
#include <set>
#include <mutex>

// BONUS another lighting model Cel Shading
//#define CELSHADING
// BONUS SuperSampling with one thread
//#define SUPERSAMPLING

// structure to encapsulate data retrieved from ray intersection with object
class RayResult {
public:
	RayResult() : result(true), object(nullptr), normal(Vec4<float>(0.0f)), point(Vec4<float>(0.0f)), color(RGB(0.f,0.f,0.f)) {}
	//true = ray hit an object, false = ray didn't hit any object
	bool result;
	//pointer to hit object
	Object* object;
	//corresponding ray
	Ray ray;
	//normal of the object in the intersection point
	Vec4<float> normal;
	//point of intersection with object
	Vec4<float> point;
	// Color of a point
	RGB color;

	RayResult& operator==(const RayResult& rhs);
};

// Context class with all needed information about any context
class Context{
public:
	// Width and height of a canvas
	int width, height;
	// Canvas where we draw all objects, one dimension with size width*height
	std::vector<RGB> colorBuffer;
	// Depth buffer holds information which is related to colorBuffer, has same size
	std::vector<float> depthBuffer;
	// Vertex buffer contains all pieces of information about vertices
	std::vector<Vec4<float>> vertexBuffer;
	// Transformations are held in these two stacks, one manifests modelview transformations
	// other projection transformations
	std::stack<Mat4<float>> transformStack[2];

	// Viewport transformation matrix for final conversion
	Mat4<float> viewMat;
	Mat4<float> PVMmat;
	// Viewport * Projection * Modelview matrix
	Mat4<float> VPPVMmat;
	
	// true if viewport / projection / modelview matrix has changed since last render
	bool PVMchanged;

	// Defines which type of element will be drawn
	sglEElementType drawingElement;
	// Which mode should be used (lines/fill/points)
	sglEAreaMode drawingMode;
	// Defines which transformStack will be altered
	sglEMatrixMode matrixMode;
	
	// Size of radius of point
	int pointRadius;

	// Color to be used as shape color
	RGB color;
	// How colorbuffer should look like before drawing any shape
	RGB clearColor;
	// Should depthTest be used during drawing
	bool deptTest;

	// Binary heap with all edges sorted in yMin
	BinaryHeap edgesTable;

	// Scene for HW III.
	Scene contextScene;
	// Mutex synchronizing supersampleset coordinats
	std::mutex M_SSS;

	//-------------Methods-----------------------------------------------------------------------------------------------
	
	// Parametric constructor, w -> width, h -> height
	Context(int w, int h);

	// Render any type of element based on vertexBuffer
	void Render();

	// Transforms the input vector by the current transformation pipeline (Modelview -> Projection -> W division -> Viewport)
	void TransformVertex(Vec4<float>& v);

	// Draw point in colorBuffer on coordinates [x1,y1]
	void DrawPoint(int x1, int y1);
	// Draw point if depthtest is on
	void DrawPointDepth(int x1, int y1, float z1);

	// Draw line in colorBuffer from point [x1,y1] to [x2,y2]
	// Uses octant independant version of Bresenham's line algorithm
	void DrawLine(int x1, int y1, int x2, int y2);
	// Draw line if depthtest is on
	void DrawLineDepth(int x1, int y1, float z1, int x2, int y2, float z2);
	// Draw circle at given coordinates with radius rad
	// Uses Bresenham's circle algorithm
	void DrawCircle(float x, float y, float z, float rad);
	// Draw ellipse in colorBuffer with center at [x,y,z] with semimajor axis a and semiminor axis b 
	void DrawEllipse(float x, float y, float z, float a, float b);

	void DrawArc(float x, float y, float z, float radius, float from, float to);

	// Set value in color buffer at given coordinates to the current drawing color
	void PutPixel(int x, int y);

	// A method that helps drawing symmetrical points on circle
	void SetSymPixel(int x0, int y0, float z0, int x, int y);

	// Updates Projection * Modelview matrix
	void UpdatePVM();

	// Updates Viewport * Projection * Modelview matrix
	void UpdateVPPVM();

	// Linear interpolation between two points
	float CalcDepth(int val1, float depth1, int val2, float depth2, int currVal);
	// Recalculates edge properties
	void RecalculateEdge(Edge & e, int y);

	// Fills an area of given buffer
	void Fill();

	// -------------------------Ray Casting HW III.------------------------------------
	// Add sphere to the scene
	void AddSphere(float x, float y, float z,float r);
	// Add light source
	void AddLight(float x, float y, float z, float r, float g, float b);
	// Set current material
	void SetMaterial();
	// Basic Phong Lighting
	RGB PhongLighting(Ray& r, Material& mat, Vec4<float> position, Vec4<float> Normal, Vec4<float> lightPos, Vec3<float> lightColor);
	// BONUS: cel shading, for usage uncomment define CELSHADING on the top of this file
	RGB CelShading(Ray& r, Object* obj, Vec4<float> position, Vec4<float> Normal);
	// Utility function for clamping vector of size 3
	void Clamp01(Vec3<float>& item);
	float clamp(float item, float min, float max);

	// Multithreading method for ray shooting
	void RayCast(unsigned start, unsigned end, const Mat4<float>& invMVPVP, const Vec4<float>& O_ray);

	// Multithreading method for preprocessing for supersampling
	void PSS(unsigned start, unsigned end, std::set<std::pair<int,int>> & superSampleSet);

	// Multithreading method for supersampling
	void MakeSuperSample(unsigned start, unsigned end, const std::set<std::pair<int, int>> & superSampleSet, const Mat4<float>& invMVPVP, const Vec4<float>& O_ray);

	// Ray shooting module of ray caster and supersampler
	RayResult ShootRay(Ray& r, unsigned depth = 0, bool refracted = false);

	// Supersample calculation method
	void SuperSample(int x, int y, const Mat4<float>& invMVPVP, const Vec4<float>& O_ray);

	//----------------------------Ray casting HW IV.----------------------------------------
	// Computes if a hitted object is occluded with another 
	bool IsOccluded(const RayResult & hit, Vec4<float>& lightPos);
	// Same as above using different parameters, also returns distance to light in parameter
	bool IsOccluded(Ray& ray, float distance);
	// Reflects ray along given normal
	Vec4<float> Reflect(const Vec4<float> ray, const Vec4<float> normal);
	// Refracts ray according to the Snells law, source: apg courseware 
	Vec4<float> Refract(const Vec4<float> & rayDir, const Vec4<float> & hitNormal, const float matIOR, const float NdotD);

	//----------------------------Ray Casting HW V.------------------------------------------
	// Computes light on hitted object from given info, lights are represented as area lights
	RGB ComputeAreaLight(RayResult& hit, Triangle& tri, EmissiveMaterial& mat);
	// Generates random float number
	float GenRandomFloat();
};
