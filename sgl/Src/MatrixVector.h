#pragma once

#include <cmath>
#define M_PI       3.14159265358979323846


// Basic template class for vectors, Vec2/3/4 inherits from this class
template<typename T = void, unsigned int vSize = 0>
class Vector {
public:
	// Size of vector
	const unsigned int size = vSize;

	// Vector values
	T vals[vSize];

	// Make vector filled with vals
	Vector(const T vals);

	// Make zero vector
	Vector() : Vector(0) {};

	~Vector() {};

	// Operator definition for vec + vec operation
	Vector operator+ (const Vector& rhs) const;

	// Operator definition for vec - vec operation
	Vector operator- (const Vector& rhs) const;

	// Operator definition for vec * vec operation
	Vector operator* (const Vector& rhs) const;

	Vector operator* (const float rhs) const;

	Vector operator/ (const float rhs) const;

	// Assignment operator
	Vector operator= (const Vector& rhs);

	bool operator==(const Vector& rhs) const;

	// Access element of values
	const T& operator[] (const int idx) const;
	T& operator[] (const int idx);

	/*
	operator +=, -=, *=?
	*/
};

// Operators for Vector class
template<typename T, unsigned int vSize>
Vector<T, vSize>::Vector(T vals) {
	for (unsigned int i = 0; i < this->size; i++) {
		this->vals[i] = vals;
	}
}

template<typename T, unsigned int vSize>
Vector<T, vSize> Vector<T, vSize>::operator+ (const Vector& rhs) const {
	//if (this->size != rhs.size) throw "ERROR: wrong vector sizes on vec+vec";

	Vector<T, vSize> tmp;
	for (unsigned int i = 0; i < this->size; i++) {
		tmp.vals[i] = this->vals[i] + rhs.vals[i];
	}
	return tmp;
}

template<typename T, unsigned int vSize>
Vector<T, vSize> Vector<T, vSize>::operator- (const Vector& rhs) const {
	//if (this->size != rhs.size) throw "ERROR: wrong vector sizes on vec-vec";

	Vector<T, vSize> tmp;
	for (unsigned int i = 0; i < this->size; i++) {
		tmp.vals[i] = this->vals[i] - rhs.vals[i];
	}
	return tmp;
}

template<typename T, unsigned int vSize>
Vector<T, vSize> Vector<T, vSize>::operator* (const Vector& rhs) const {
	//if (this->size != rhs.size) throw "ERROR: wrong vector sizes on vec*vec";

	Vector<T, vSize> tmp;
	for (unsigned int i = 0; i < this->size; i++) {
		tmp.vals[i] = this->vals[i] * rhs.vals[i];
	}
	return tmp;
}

template<typename T, unsigned int vSize>
Vector<T, vSize> Vector<T, vSize>::operator/ (const float rhs) const {

	Vector<T, vSize> tmp;
	for (unsigned int i = 0; i < this->size; i++) {
		tmp.vals[i] = this->vals[i] / rhs;
	}
	return tmp;
}

template<typename T, unsigned int vSize>
Vector<T, vSize> Vector<T, vSize>::operator* (const float rhs) const {

	Vector<T, vSize> tmp;
	for (unsigned int i = 0; i < this->size; i++) {
		tmp.vals[i] = this->vals[i] * rhs;
	}
	return tmp;
}

template<typename T, unsigned int vSize>
Vector<T, vSize> Vector<T, vSize>::operator=(const Vector& rhs) {
	//if (this->size != rhs.size) throw "ERROR: wrong vector sizes on vec assignment";

	for (unsigned int i = 0; i < this->size; i++) {
		this->vals[i] = rhs.vals[i];
	}
	return *this;
}

template<typename T, unsigned int vSize>
bool Vector<T, vSize>::operator==(const Vector& rhs) const {
	for (unsigned int i = 0; i < this->size; i++) {
		if (this->vals[i] != rhs.vals[i]) return false;
	}
	return true;
}


template<typename T, unsigned int vSize>
const T& Vector<T, vSize>::operator[](const int idx) const {
	return vals[idx];
}

template<typename T, unsigned int vSize>
T& Vector<T, vSize>::operator[](const int idx) {
	return vals[idx];
}

// Vec2 class derivation
template<typename T = void>
class Vec2 : public Vector<T, 2> {
public:
	// Default constructors are handled by Vector class
	Vec2() {};
	Vec2(const Vector<T, 2>& v) : Vector<T, 2>(v) {}

	// Vector defined by all values
	Vec2(T v1, T v2);

	//Vec2(Vec3<T>&v); // make vec2 from vec3
};

template<typename T>
Vec2<T>::Vec2(T v1, T v2){ //: this->vals[0](v1), this->vals[1](v2) {
	T arr[2] = { v1, v2 };
	for (unsigned int i = 0; i < this->size; i++) this->vals[i] = arr[i];
}

// Vec3 class derivation
template<typename T = void>
class Vec3 : public Vector<T, 3> {
public:
	// Default constructors are handled by Vector class
	Vec3() {};
	Vec3(const Vector<T, 3>& v) : Vector<T, 3>(v) {}
	Vec3(const Vector<T, 4>& v);

	// Vector defined by all values
	Vec3(T v1, T v2, T v3);

	// Dot product of two vectors
	float DotProd(const Vec3<T> other) const;
	// Cross product of two vectors
	Vec3<T> CrossProd(const Vec3<T> other) const;
	// Normalize vector
	void Normalize();
	// Return lenght of the vector
	float Length();

	//Vec3(Vec4<T>& v); // make vec3 from vec4
	//Vec3(Vec2<T>& v, v3); // make vec3 from vec2 + value
};

template<typename T>
Vec3<T>::Vec3(const Vector<T, 4>& v) {
	this->vals[0] = v.vals[0];
	this->vals[1] = v.vals[1];
	this->vals[2] = v.vals[2];
}

template<typename T>
Vec3<T> Vec3<T>::CrossProd(const Vec3<T> other) const {
	float v0 = (this->vals[1]*other.vals[2])-(this->vals[2]*other.vals[1]);
	float v1 = (this->vals[2]*other.vals[0])-(this->vals[0]*other.vals[2]);
	float v2 = (this->vals[0]*other.vals[1])-(this->vals[1]*other.vals[0]);
	return Vec3<T>(v0,v1,v2);
}

template<typename T>
float Vec3<T>::DotProd(const Vec3<T> other) const {
	return this->vals[0]*other.vals[0] + this->vals[1] * other.vals[1] + this->vals[2] * other.vals[2];
}


template<typename T>
Vec3<T>::Vec3(T v1, T v2, T v3) {
	T arr[3] = { v1, v2, v3 };
	for (unsigned int i = 0; i < this->size; i++) this->vals[i] = arr[i];
}

// Vec4 class derivation
template<typename T = void>
class Vec4 : public Vector<T, 4> {
public:
	// Default constructors are handled by Vector class
	Vec4() {};
	Vec4(const Vector<T, 4>& v) : Vector<T, 4>(v) {}

	// Vector defined by all values
	Vec4(T v1, T v2, T v3, T v4);

	// Make vec4 from vec3 + value
	Vec4(Vec3<T>& v, T v4);

	// Dot product of two vectors
	float DotProd(const Vec4<T> other) const;
	// Cross product of two vectors
	Vec4<T> CrossProd(const Vec4<T> other) const;
	// Normalize vector
	void Normalize();
	// Return lenght of the vector
	float Lenght();

	// W division
	void DivideW();
};

template<typename T>
float Vec4<T>::Lenght() {
	return sqrt(this->vals[0] * this->vals[0] + this->vals[1] * this->vals[1] + this->vals[2] * this->vals[2]);
}

template<typename T>
void Vec4<T>::Normalize() {
	float den = Lenght();
	this->vals[0] = this->vals[0] / den;
	this->vals[1] = this->vals[1] / den;
	this->vals[2] = this->vals[2] / den;
}

template<typename T>
Vec4<T> Vec4<T>::CrossProd(const Vec4<T> other) const {
	float v0 = (this->vals[1] * other.vals[2]) - (this->vals[2] * other.vals[1]);
	float v1 = (this->vals[2] * other.vals[0]) - (this->vals[0] * other.vals[2]);
	float v2 = (this->vals[0] * other.vals[1]) - (this->vals[1] * other.vals[0]);
	return Vec4<T>(v0, v1, v2, 0);
}

template<typename T>
float Vec4<T>::DotProd(const Vec4<T> other) const {
	return this->vals[0] * other.vals[0] + this->vals[1] * other.vals[1] + this->vals[2] * other.vals[2];
}

template<typename T>
void Vec4<T>::DivideW() {
	this->vals[0] /= this -> vals[3];
	this->vals[1] /= this -> vals[3];
	this->vals[2] /= this -> vals[3];
	this->vals[3] /= this -> vals[3];
}

template<typename T>
Vec4<T>::Vec4(T v1, T v2, T v3, T v4) {
	T arr[4] = { v1, v2, v3, v4 };
	for (unsigned int i = 0; i < this->size; i++) this->vals[i] = arr[i];
	
}

template<typename T>
Vec4<T>::Vec4(Vec3<T>& v, T v4) {
	for (unsigned int i = 0; i < v.size; i++) this->vals[i] = v.vals[i];
	this->vals[3] = v4;
}

// Basic Template class for matricies
template<typename T = void, unsigned int mSize = 0>
class Matrix {
public:
	// Size of a new matrix in m x m
	const unsigned int size = mSize;

	// Matrix values array
	T vals[mSize][mSize];

	// Creates an identity matrix
	Matrix();

	// Matrix with diag value on diagonal
	Matrix(T diag);

	// Copy constructor
	Matrix(const Matrix& m);

	Matrix(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9,
		T v10, T v11, T v12, T v13, T v14, T v15, T v16);

	// Destructor
	~Matrix() {};

	// Definition of mat + mat operation
	Matrix operator+ (const Matrix& rhs) const;

	// Definition of mat - mat operation
	Matrix operator- (const Matrix& rhs) const;

	// Definition of mat * mat operation
	Matrix operator* (const Matrix& rhs) const;

	// Definition of scalar * mat operation
	Matrix operator* (const float scalar) const;

	// Assignment operator
	Matrix operator= (const Matrix& rhs);

	// Definition of mat * vec operation
	Vector<T, mSize> operator* (const Vector<T, mSize>& rhs) const;

	// Value at mat(row,col)
	T& operator() (const int row, const int col);

	// Compares matrices by elements
	bool operator== (const Matrix& rhs) const;

	// Compares sizes of matrices
	bool EqSize(const Matrix& m) const;

	/*
	operator +=, -=, *=?
	*/
};

// Matrix 3x3 derivation
template<typename T = void>
class Mat3 : public Matrix<T, 3>
{
public:
	// Identity matrix (handled by Matrix class)
	Mat3() {};

	// Matrix copy constuctor
	Mat3(const Matrix<T, 3>& m) : Matrix<T, 3>(m) {}

	// Matrix defined by all values
	Mat3(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9);


	// Matrix defined by diagonal values
	Mat3(T v1, T v2, T v3);

	//vec3 getrow(i), vec3 getcolumn(i) // returns vector at given row/column
	// Mat3(Vec3 v1, Vec3 v2, Vec3 v3) ? // constructs matrix from vectors (column order)
};

// Matrix 4x4 derivation
template<typename T = void>
class Mat4 : public Matrix <T, 4>
{
public:
	// Identity matrix (handled by Matrix class)
	Mat4() {};

	// Copy construcotr of 4x4 matrix
	Mat4(const Matrix<T, 4>& m) : Matrix<T, 4>(m) {}

	// Matrix defined by all values
	Mat4(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9,
		T v10, T v11, T v12, T v13, T v14, T v15, T v16);


	// Matrix defined by diagonal
	Mat4(T v1, T v2, T v3, T v4);

	// vec4 getrow(i), vec4 getcolumn(i) // returns vector at given row/column
	// Mat4(Vec4 v1, Vec4 v2, Vec4 v3, Vec4 v4) // constructs matrix from vectors (column order)
};
 
template<typename T, unsigned int mSize>
Matrix<T, mSize>::Matrix() {
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			(i == j) ? this->vals[i][j] = 1 : this->vals[i][j] = 0;
		}
	}
}

template<typename T, unsigned int mSize>
Matrix<T, mSize>::Matrix(T diag) {
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			(i == j) ? this->vals[i][j] = diag : this->vals[i][j] = 0;
		}
	}
}

template<typename T, unsigned int mSize>
Matrix<T, mSize>::Matrix(const Matrix& m) {
	//if (this->size != m.size) throw "ERROR: wrong matrix sizes on copy constructor";

	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			this->vals[i][j] = m.vals[i][j];
		}
	}
}

template<typename T, unsigned int mSize>
Matrix<T, mSize>::Matrix(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9,
	T v10, T v11, T v12, T v13, T v14, T v15, T v16) : vals{ {v1, v5, v9, v13}, { v2, v6, v10, v14 }, { v3, v7, v11, v15 }, { v4, v8, v12, v16 } }
{}
// : vals{ {v1, v5, v9, v13}, { v1, v5, v9, v13 }, { v1, v5, v9, v13 }, { v1, v5, v9, v13 } }

// Matrix operators definitions
template<typename T, unsigned int mSize>
Matrix<T, mSize> Matrix<T, mSize>::operator+ (const Matrix& rhs) const {
	//if (this->size != rhs.size) throw "ERROR: wrong matrix sizes on mat+mat";
	Matrix<T, mSize> tmp;

	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			tmp.vals[i][j] = this->vals[i][j] + rhs.vals[i][j];
		}
	}
	return tmp;
}

template<typename T, unsigned int mSize>
Matrix<T, mSize> Matrix<T, mSize>::operator-(const Matrix& rhs) const {
	if (this->size != rhs.size) throw "ERROR: wrong matrix sizes on mat-mat";
	Matrix<T, mSize> tmp;

	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			tmp.vals[i][j] = this->vals[i][j] - rhs.vals[i][j];
		}
	}
	return tmp;
}

template<typename T, unsigned int mSize>
Matrix<T, mSize> Matrix<T, mSize>::operator*(const Matrix& rhs) const {
	//if (this->size != rhs.size) throw "ERROR: wrong matrix sizes on mat*mat";
	Matrix<T, mSize> tmp(0);

	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			for (unsigned int k = 0; k < this->size; k++) {
				tmp.vals[i][j] += this->vals[i][k] * rhs.vals[k][j];
			}
		}
	}
	return tmp;
}

template<typename T, unsigned int mSize>
Vector<T, mSize> Matrix<T, mSize>::operator*(const Vector<T, mSize>& rhs) const {
	//if (this->size != rhs.size) throw "ERROR: wrong sizes on mat*vec";
	Vector<T, mSize> tmp;
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			tmp[i] += this->vals[i][j] * rhs[j];
		}
	}
	return tmp;
}

template<typename T, unsigned int mSize>
Matrix<T, mSize> Matrix<T, mSize>::operator=(const Matrix<T, mSize>& rhs) {
	//if (this->size != rhs.size) throw "ERROR: wrong sizes on mat assignment";

	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			this->vals[i][j] = rhs.vals[i][j];
		}
	}
	return *this;
}

template<typename T, unsigned int mSize>
T& Matrix<T, mSize>::operator()(const int row, const int col) {
	return this->vals[row][col];
}

template<typename T, unsigned int mSize>
bool Matrix<T, mSize>::operator==(const Matrix& rhs) const {
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			if (this->vals[i][j] != rhs.vals[i][j]) return false;
		}
	}
	return true;
}

template<typename T, unsigned int mSize>
bool Matrix<T, mSize>::EqSize(const Matrix& m) const {
	return (this->size == m.size) ? true : false;
}


template<typename T, unsigned int mSize>
Matrix<T, mSize> Matrix<T, mSize>::operator*(const float scalar) const {
	Matrix<T, mSize> tmp(*this);
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			tmp.vals[i][j] *= scalar;
		}
	}
	return tmp;
}

// Constructors for derivated classes
template<typename T>
Mat3<T>::Mat3(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9) {
	T arr[9] = { v1, v2, v3, v4, v5, v6, v7, v8, v9 };
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			this->vals[j][i] = arr[i * this->size + j];
		}
	}
}

template<typename T>
Mat3<T>::Mat3(T v1, T v2, T v3) {
	T arr[3] = { v1, v2, v3 };
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			(i == j) ? this->vals[i][j] = arr[i] : this->vals[i][j] = 0;
		}
	}
}

template<typename T>
Mat4<T>::Mat4(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9,
	T v10, T v11, T v12, T v13, T v14, T v15, T v16) : Matrix<T,4>(v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16)
{
	/*
	T arr[16] = { v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16 };
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			this->vals[j][i] = arr[i * this->size + j];
		}
	}
	*/
}

template<typename T>
Mat4<T>::Mat4(T v1, T v2, T v3, T v4) {
	T arr[4] = { v1, v2, v3, v4 };
	for (unsigned int i = 0; i < this->size; i++) {
		for (unsigned int j = 0; j < this->size; j++) {
			(i == j) ? this->vals[i][j] = arr[i] : this->vals[i][j] = 0;
		}
	}
}

template<typename T, unsigned int mSize>
int Invert(Matrix<T, mSize>& tmp)
{
	Matrix<T, mSize> x = tmp;
	int indxc[4], indxr[4], ipiv[4];
	int i, icol, irow, j, k, l, ll, n;
	float big, dum, pivinv, temp;
	// satisfy the compiler
	icol = irow = 0;

	// the size of the matrix
	n = mSize;

	for (j = 0; j < n; j++) /* zero pivots */
		ipiv[j] = 0;

	for (i = 0; i < n; i++)
	{
		big = 0.0;
		for (j = 0; j < n; j++)
			if (ipiv[j] != 1)
				for (k = 0; k < n; k++)
				{
					if (ipiv[k] == 0)
					{
						if (abs(x(k, j)) >= big)
						{
							big = abs(x(k, j));
							irow = j;
							icol = k;
						}
					}
					else
						if (ipiv[k] > 1)
							return 0; /* singular matrix */
				}
		++(ipiv[icol]);
		if (irow != icol)
		{
			for (l = 0; l < n; l++)
			{
				temp = x(l, icol);
				x(l, icol) = x(l, irow);
				x(l, irow) = temp;
			}
		}
		indxr[i] = irow;
		indxc[i] = icol;
		if (x(icol, icol) == 0.0)
			return 0; /* singular matrix */

		pivinv = 1.0f / x(icol, icol);
		x(icol, icol) = 1.0;
		for (l = 0; l < n; l++)
			x(l, icol) = x(l, icol) * pivinv;

		for (ll = 0; ll < n; ll++)
			if (ll != icol)
			{
				dum = x(icol, ll);
				x(icol, ll) = 0.0;
				for (l = 0; l < n; l++)
					x(l, ll) = x(l, ll) - x(l, icol) * dum;
			}
	}
	for (l = n; l--; )
	{
		if (indxr[l] != indxc[l])
			for (k = 0; k < n; k++)
			{
				temp = x(indxr[l], k);
				x(indxr[l], k) = x(indxc[l], k);
				x(indxc[l], k) = temp;
			}
	}

	tmp = x;
	return 1; // matrix is regular .. inversion has been succesfull
}

template<typename T, unsigned int vSize>
float Magnitude(Vector<T, vSize>& v) {
	float res = 0.0f;
	//vSize-1 -- don't use the last value for normalization (w component) -- this is really bad approach though!
	for (unsigned i = 0; i < vSize-1; i++) {
		res += pow(v[i], 2);
	}
	return sqrt(res);
}


template<typename T, unsigned int vSize>
Vector<T, vSize> Normalize(Vector<T, vSize> v) {
	v = v / Magnitude(v);
	return v;
}