#include "SceneRes.h"
#include <algorithm>

float RGB::Length() {
	return sqrt(r*r + g*g + b*b);
}

void RGB::Normalize() {
	float den = Length();
	r = r / den;
	g = g / den;
	b = b / den;
}

void RGB::Clamp() {
	r = std::max(0.0f, std::min(r, 1.0f));
	g = std::max(0.0f, std::min(g, 1.0f));
	b = std::max(0.0f, std::min(b, 1.0f));
}

RGB RGB::operator+ (const RGB& rhs) const {
	RGB res;
	res.r = this->r + rhs.r;
	res.g = this->g + rhs.g;
	res.b = this->b + rhs.b;
	return res;
}

RGB RGB::operator- (const RGB& rhs) const {
	RGB res;
	res.r = this->r - rhs.r;
	res.g = this->g - rhs.g;
	res.b = this->b - rhs.b;
	return res;
}

RGB RGB::operator* (const float rhs) const {
	RGB res;
	res.r = this->r * rhs;
	res.g = this->g * rhs;
	res.b = this->b * rhs;
	return res;
}

RGB RGB::operator/ (const float rhs) const {
	RGB res;
	res.r = this->r / rhs;
	res.g = this->g / rhs;
	res.b = this->b / rhs;
	return res;
}

bool Sphere::Intersection(const Ray& ray, float& t, int& idx) {
	Vec4<float> dst = ray.origin - position;
	float b = dst.DotProd(ray.dir);
	float c = dst.DotProd(dst) - radius * radius;
	float d = b * b - c;

	if (d > 0) {
		float sqrtd = sqrtf(d);
		t = -b - sqrtd;
		if (t < 0.0f)
			t = -b + sqrtd;

		// both intersections negative => sphere is behind the ray origin
		if (t < 0.0f) return false;

		return true;
	}
	return false;
}

Vec4<float> Sphere::GetNormal(Vec4<float>& intersectionP, int triangleIdx) {
	return Normalize(intersectionP - position);
}

bool TriangleMesh::Intersection(const Ray& ray, float& t, int& idx) {
	float t_min = std::numeric_limits<float>::max();
	//here save the index of the closest triangle, return it from the function for normal calculation in shading
	for (unsigned int i = 0; i < triangles.size(); i++) {
		float t_curr = 0.0f;
		if (triangles[i].Intersection(ray, t_curr)) {
			Vec4<float> hitPoint = ray.origin + (ray.dir * t_curr);
			Vec4<float> hitNormal = GetNormal(hitPoint, i);
			if (hitNormal.DotProd(ray.dir) > 0.0f) continue;
			if (t_curr < 0.1f) continue;

			//if parameter t is positive (in direction of ray dir) and less than current min
			if (t_curr > 0.0f && t_curr < t_min) {
				t_min = t_curr;
				idx = i;
			}
		}
	}
	//if min is still max value, didn't intersect anything
	if (t_min == std::numeric_limits<float>::max()) {
		return false;
	}
	//else return the minimum intersection point;
	t = t_min;
	return true;
}

Vec4<float> TriangleMesh::GetNormal(Vec4<float>& intersectionP, int triangleIdx) {
	Vec4<float> e1 = triangles[triangleIdx].vertices[1] - triangles[triangleIdx].vertices[0];
	Vec4<float> e2 = triangles[triangleIdx].vertices[2] - triangles[triangleIdx].vertices[0];
	return Normalize(e1.CrossProd(e2));
}

Triangle::Triangle(Vec4<float> v0, Vec4<float> v1, Vec4<float> v2) {
	vertices[0] = v0;
	vertices[1] = v1;
	vertices[2] = v2;
}

bool Triangle::Intersection(const Ray& ray, float& t) {
	// Initialize triangle intersection statistics
	//static StatsPercentage triangleHits("Geometry", "Triangle Ray Intersections");
	// Update triangle tests count
	//triangleHits.Add(0, 1);
	// Compute $\VEC{s}_1$
	// Get triangle vertices in _p1_, _p2_, and _p3_
	float EPS = 0.0000001f;
	Vec4<float>& p1 = vertices[0];
	Vec4<float>& p2 = vertices[1];
	Vec4<float>& p3 = vertices[2];
	Vec4<float> e1 = p2 - p1;
	Vec4<float> e2 = p3 - p1;
	Vec4<float> s1 = ray.dir.CrossProd(e2);
	float divisor = s1.DotProd(e1);
	if (divisor == 0.)
		return false;
	float invDivisor = 1.f / divisor;
	// Compute first barycentric coordinate
	Vec4<float> d = ray.origin - p1;
	float b1 = d.DotProd(s1) * invDivisor;
	if (b1 < 0. || b1 > 1.)
		return false;
	// Compute second barycentric coordinate
	Vec4<float> s2 = d.CrossProd(e1);
	float b2 = ray.dir.DotProd(s2) * invDivisor;
	if (b2 < 0. || b1 + b2 > 1.)
		return false;
	// Compute _t_ to intersection point
	float tmpT = e2.DotProd(s2) * invDivisor;
	//if (tmpT < ray.mint || tmpT > ray.maxt)
	if (tmpT > EPS) {
		t = tmpT;
		return true;
	}
	return false;

}

void Scene::SetBGEnv(const int w, const int h, float* texs) {
	// Set basic resolution
	bgEnv.width = w;
	bgEnv.height = h;
	bgEnv.size = w * h * 3;
	// Load all texels
	//printf("Size is %d %d\n", w, h);
	for (int i = 0; i < bgEnv.size; i++)
		bgEnv.texels.push_back(texs[i]);
	//printf("Size of loaded texs is %u\n", bgEnv.texels.size());
}

RGB Scene::GetBGEnv(const Ray& ray) {
	// Check if there is any env map
	if (bgEnv.width == -1)
		return RGB(0.f, 0.f, 0.f);
	// Compute what to put on given place
	float c = sqrt(ray.dir.vals[0]* ray.dir.vals[0] + ray.dir.vals[1]* ray.dir.vals[1]);
	float r = (c > 0) ? (float)(acos(ray.dir.vals[2])/(2*c*M_PI)) : 0.0f;
	float u = 0.5f + r * ray.dir.vals[0]; //x
	float v = 0.5f + r * ray.dir.vals[1]; //y
	int texX = (int)(u * bgEnv.width);
	int texY = (int)(v * bgEnv.height);
	int texIdx = bgEnv.size - (texY * bgEnv.width * 3) + texX * 3;
	// Compute and return correct color
	return RGB(bgEnv.texels[texIdx], bgEnv.texels[texIdx + 1], bgEnv.texels[texIdx + 2]);
}


void TriangleMesh::PrintInfo() {
	printf("  Class type: TriangleMesh\n");
	printf("   material rgb: [%f, %f, %f]\n", mat.color[0], mat.color[1], mat.color[2]);
	printf("   material coefs: [%f, %f, %f, %f, %f]\n", mat.kd, mat.ks, mat.shine, mat.ior, mat.T);
	printf("   triangle cnt: %I64u\n", triangles.size());
	for (auto a : triangles) {
		printf("     [%f, %f, %f], [%f, %f, %f], [%f, %f, %f]\n", 
			a.vertices[0][0], a.vertices[0][1], a.vertices[0][2], 
			a.vertices[1][0], a.vertices[1][1], a.vertices[1][2], 
			a.vertices[2][0], a.vertices[2][1], a.vertices[2][2]);
	}
}

void Sphere::PrintInfo() {
	printf("  Class type: Sphere\n");
	printf("   material rgb: [%f, %f, %f]\n", mat.color[0], mat.color[1], mat.color[2]);
	printf("   material coefs: [kd: %f, ks: %f, shine: %f, ior: %f, T: %f]\n", mat.kd, mat.ks, mat.shine, mat.ior, mat.T);
	printf("   center: [%f, %f, %f], radius: %f\n", position[0], position[1], position[2], radius);
}