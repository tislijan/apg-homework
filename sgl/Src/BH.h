#pragma once
#include <vector>
#include "MatrixVector.h"

// Edge struct for fill algorithm
struct Edge {
    // Start of an edge
    int yMin;
    // End of an edge
    int yMax;
    // Current x star position
    float x;
    // X coordinate of yMax
    int xMax;
    // Z coordinate of yMin
    float zMin;
    // Z coordinate of yMax
    float zMax;
    // slope of an edge
    float slope;

    // edge parameterless constructor
    Edge();
    // parametric constructor
    Edge(const Vec4<float> & v1, const Vec4<float> & v2);
    // overloaded compare
    bool operator<(const Edge& e2);
};

// Binary Heap for fast edge search
class BinaryHeap {
private:
    // Maximal item
    int max;

public:
    // Vector of edges, heap si represented as array
    std::vector<Edge> edges;
    // Constructor
    BinaryHeap() : max(0) {}
    // Destructor
    ~BinaryHeap() {}

    // Compare function for correct order
    bool CompareIdxs(int parent, int child);

    // Is heap empty?
    bool Empty();

    // Add new item to heap
    void Add(Edge key);

    // Correct heap
    void MinHeapify(int i);

    // Get minimum from heap
    Edge extractMin();

    // Swap two items
    void swapBin(const int& par, const int& ch);

    // Print whole queue
    void PrintQueue();

};

