#include "ContextBuffers.h"
#include "MatrixVector.h"
#include "BH.h"
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>
#include <random>

#define M_PI       3.14159265358979323846
#define SHADOWBIAS 0.00005f
#define MAXDEPTH 8

using namespace std;

RayResult& RayResult::operator==(const RayResult& rhs) {
    if (this == &rhs) return *this;

    this->color = rhs.color;
    this->normal = rhs.normal;
    this->object = rhs.object;
    this->point = rhs.point;
    this->ray = rhs.ray;
    this->result = rhs.result;
    return *this;
}

Context::Context(int w, int h) :width(w), height(h) {
	deptTest = false;
	drawingElement = sglEElementType(0);
	drawingMode = SGL_POINT;
	matrixMode = SGL_MODELVIEW;
    PVMchanged = false;

	colorBuffer.resize(width * height);

#ifndef NO_RASTERIZATION
    pointRadius = 0;

	depthBuffer.resize(width * height);
    // Fills depth buffer with high float value
    fill(depthBuffer.begin(), depthBuffer.end(), std::numeric_limits<float>::max());
#endif // !NO_RASTERIZATION

	color = RGB(1.0f, 1.0f, 1.0f);
	clearColor = RGB(1.0f, 1.0f, 1.0f);
}

void Context::TransformVertex(Vec4<float>& v) {
    //v = PVMmat * v;
    //v = viewMat * v;
    v = VPPVMmat * v;
    //if (v[3] != 1) 
        v.DivideW();
}

void Context::Render() {
    if (PVMchanged) {
        UpdateVPPVM();
        PVMchanged = false;
    }
    // Transform vertices in vertex buffer with MV, P, VP transforms
    for (auto& it : vertexBuffer) {
        TransformVertex(it);
        //printf("[%f, %f, %f, %f]\n", it[0], it[1], it[2], it[3]);
    }

    // Render primitives according to drawing mode
    if (drawingElement == SGL_POINTS) {
        if (!deptTest) {
            for (unsigned int i = 0; i < vertexBuffer.size(); i++) {
                DrawPoint((int)vertexBuffer[i][0], (int)vertexBuffer[i][1]);
            }
        }
        else {
            for (unsigned int i = 0; i < vertexBuffer.size(); i++) {
                DrawPointDepth(vertexBuffer[i][0], vertexBuffer[i][1], vertexBuffer[i][2]);
            }
        }
    }
    else if (drawingElement == SGL_LINES) {
        if (!(vertexBuffer.size() > 1) || vertexBuffer.size() % 2 != 0) return; //what error should be thrown?
        if (!deptTest) {
            for (unsigned int i = 0; i < vertexBuffer.size(); i += 2) {
                DrawLine(vertexBuffer[i][0], vertexBuffer[i][1], vertexBuffer[i + 1][0], vertexBuffer[i + 1][1]);
            }
        }
        else {
            for (unsigned int i = 0; i < vertexBuffer.size(); i += 2) {
                DrawLineDepth(vertexBuffer[i][0], vertexBuffer[i][1], vertexBuffer[i][2], vertexBuffer[i + 1][0], vertexBuffer[i + 1][1], vertexBuffer[i+1][2]);
            }

        }
    }
    else if (drawingElement == SGL_LINE_STRIP) {
        if (!deptTest) {
            for (unsigned int i = 1; i < vertexBuffer.size(); i++) {
                DrawLine(vertexBuffer[i - 1][0], vertexBuffer[i - 1][1], vertexBuffer[i][0], vertexBuffer[i][1]);
            }
        }
        else {
            for (unsigned int i = 1; i < vertexBuffer.size(); i++) {
                DrawLineDepth(vertexBuffer[i - 1][0], vertexBuffer[i - 1][1], vertexBuffer[i - 1][2], vertexBuffer[i][0], vertexBuffer[i][1], vertexBuffer[i][2]);
            }
        }  
    }
    else if (drawingElement == SGL_LINE_LOOP) {
        if (!deptTest) {
            for (unsigned int i = 1; i < vertexBuffer.size(); i++) {
                DrawLine(vertexBuffer[i - 1][0], vertexBuffer[i - 1][1], vertexBuffer[i][0], vertexBuffer[i][1]);
            }
            if (!(vertexBuffer.size() > 2)) return; //only 2 vertices, would be drawn twice
            DrawLine(vertexBuffer[0][0], vertexBuffer[0][1], vertexBuffer.back()[0], vertexBuffer.back()[1]);
        }
        else {
            for (unsigned int i = 1; i < vertexBuffer.size(); i++) {
                DrawLineDepth(vertexBuffer[i - 1][0], vertexBuffer[i - 1][1], vertexBuffer[i - 1][2], vertexBuffer[i][0], vertexBuffer[i][1], vertexBuffer[i][2]);
            }
            DrawLineDepth(vertexBuffer[0][0], vertexBuffer[0][1], vertexBuffer[0][2], vertexBuffer.back()[0], vertexBuffer.back()[1], vertexBuffer.back()[2]);
        }
        
    }
    else if (drawingElement == SGL_TRIANGLES) {
        //TODO in future HW
    }
    else if (drawingElement == SGL_POLYGON) {
        if (!deptTest) {
            for (unsigned int i = 1; i < vertexBuffer.size(); i++) {
                DrawLine(vertexBuffer[i - 1][0], vertexBuffer[i - 1][1], vertexBuffer[i][0], vertexBuffer[i][1]);
                if (drawingMode == SGL_FILL && (int)vertexBuffer[i - 1][1] != (int)vertexBuffer[i][1]) {
                    edgesTable.Add(Edge(vertexBuffer[i - 1], vertexBuffer[i]));
                }
            }
            DrawLine(vertexBuffer[0][0], vertexBuffer[0][1], vertexBuffer.back()[0], vertexBuffer.back()[1]);
            if (drawingMode == SGL_FILL && (int)vertexBuffer[0][1] != (int)vertexBuffer.back()[1]) {
                edgesTable.Add(Edge(vertexBuffer[0], vertexBuffer.back()));
            }
            if (drawingMode == SGL_FILL)
                Fill();
        }
        else {
            for (unsigned int i = 1; i < vertexBuffer.size(); i++) {
                DrawLineDepth(vertexBuffer[i - 1][0], vertexBuffer[i - 1][1], vertexBuffer[i - 1][2], vertexBuffer[i][0], vertexBuffer[i][1], vertexBuffer[i][2]);
                if (drawingMode == SGL_FILL && (int)vertexBuffer[i - 1][1] != (int)vertexBuffer[i][1]) {
                    edgesTable.Add(Edge(vertexBuffer[i - 1], vertexBuffer[i]));
                }
            }
            DrawLineDepth(vertexBuffer[0][0], vertexBuffer[0][1], vertexBuffer[0][2], vertexBuffer.back()[0], vertexBuffer.back()[1], vertexBuffer.back()[2]);
            if (drawingMode == SGL_FILL && (int)vertexBuffer[0][1] != (int)vertexBuffer.back()[1]) {
                edgesTable.Add(Edge(vertexBuffer[0], vertexBuffer.back()));
            }
            if (drawingMode == SGL_FILL)
                Fill();
        }
        
    }
}

void Context::DrawPoint(int x1, int y1) {
    int r = pointRadius;
    
    for (int x = -r; x <= r; x++) {
        for (int y = -r; y <= r; y++) {
            PutPixel(x1 + x, y1 + y);
        }
    }
}

void Context::DrawPointDepth(int x1, int y1, float z1) {
    int r = pointRadius;

    for (int x = -r; x <= r; x++) {
        for (int y = -r; y <= r; y++) {
            if (z1 <= depthBuffer[y1 * width + x1]) {
                depthBuffer[y1 * width + x1] = z1;
                PutPixel(x1, y1);
            }
        }
    }
}

void Context::DrawLine(int x1, int y1, int x2, int y2) {
    int w = x2 - x1;
    int h = y2 - y1;
    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
    if (w < 0) {
        dx1 = -1;
        dx2 = -1;
    }
    else if (w > 0) {
        dx1 = 1;
        dx2 = 1;
    }
    if (h < 0) dy1 = -1;
    else if (h > 0) dy1 = 1;

    int max = abs(w);
    int min = abs(h);
    if (!(max > min)) {
        std::swap(min, max);
        if (h < 0) dy2 = -1;
        else if (h > 0) dy2 = 1;
        dx2 = 0;
    }
    int num = max / 2;
    for (int i = 0; i <= max; i++) {
        PutPixel(x1, y1);
        num += min;
        if (!(num < max)) {
            num -= max;
            x1 += dx1;
            y1 += dy1;
        }
        else {
            x1 += dx2;
            y1 += dy2;
        }
    }
}

void Context::DrawLineDepth(int x1, int y1, float z1, int x2, int y2, float z2) {

    // Z-buffer variables
    bool leadX = true;
    int origX1 = x1, origY1 = y1, origX2 = x2, origY2 = y2;
    //z1 = 1 / z1;
    //z2 = 1 / z2;

    int w = x2 - x1;
    int h = y2 - y1;
    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
    if (w < 0) {
        dx1 = -1;
        dx2 = -1;
    }
    else if (w > 0) {
        dx1 = 1;
        dx2 = 1;
    }
    if (h < 0) dy1 = -1;
    else if (h > 0) dy1 = 1;

    int max = abs(w);
    int min = abs(h);
    if (!(max > min)) {
        std::swap(min, max);
        leadX = false; // Z-buffer
        if (h < 0) dy2 = -1;
        else if (h > 0) dy2 = 1;
        dx2 = 0;
    }
    int num = max / 2;
    for (int i = 0; i <= max; i++) {
            float z = 1;
            if (leadX) z = CalcDepth(origX1, z1, origX2, z2, x1);
            else       z = CalcDepth(origY1, z1, origY2, z2, y1);

            if (z <= depthBuffer[y1 * width + x1]) {
                depthBuffer[y1 * width + x1] = z;
                PutPixel(x1, y1);
            }

        num += min;
        if (!(num < max)) {
            num -= max;
            x1 += dx1;
            y1 += dy1;
        }
        else {
            x1 += dx2;
            y1 += dy2;
        }
    }
}

void Context::DrawCircle(float xs, float ys, float zs, float rad) {
    // Bresenham circle algorithm
    int x = 0;
    int y = (int)rad;
    int p = 3 - (2 * rad);
    while (x <= y) {
        SetSymPixel(xs, ys, zs, x, y);
        if (p < 0) {
            p = p + 4 * x + 6;
        }
        else {
            p = p + 4 * (x - y) + 10;
            y--;
        }
        x++;
    }

    // also draw zs value to depth buffer on target pixels?
}

void Context::DrawEllipse(float x, float y, float z, float a, float b) {
    sglEElementType bk = drawingElement;
    // putTo signalizes if we have to copy computed points two or four times
    // mid means middle point of preallocated vertexbuffer,
    // it has same values as computed point with only different signs
    // end stand for last item in vertex buffer, where we dont write,
    // but uses its value to write verticies from end (symetrical verticies)
    int putTo = 0, mid = 20, end = 40;
    vertexBuffer.resize(40);

    // compute only first 10 degrees and from them x and y values, save them 
    // with respective positions and signs in vertex buffer
    for (int i = 0; i <= 90; i += 9) {
        float degree = i * 2 * M_PI / 360;
        float ex = x + a * cos(degree);
        float ey = y + b * sin(degree);
        float dx = ex - x;
        float dy = ey - y;

        if (putTo == 0) { // special case, we want to use this point on two places - start and middle point -> x=a, y=0 && x=-a, y=0
            vertexBuffer[putTo] = Vec4<float>(ex, ey, z, 1);
            vertexBuffer[putTo + mid] = Vec4<float>(x-dx, y+dy, z, 1);
        }
        else if (putTo == 10) { // another special case, x=0, y=b, here we want to copy that point only once to x=0, y=-b
            vertexBuffer[putTo] = Vec4<float>(ex, ey, z, 1);
            vertexBuffer[putTo + mid] = Vec4<float>(x-dx, y-dy, z, 1);
        }
        else {
            vertexBuffer[putTo] = Vec4<float>(ex,ey,z,1);
            vertexBuffer[mid - putTo] = Vec4<float>(x-dx,y+dy,z,1);
            vertexBuffer[mid + putTo] = Vec4<float>(x-dx,y-dy,z,1);
            vertexBuffer[end - putTo] = Vec4<float>(x+dx,y-dy,z,1);
        }
        putTo++;
    }
    drawingElement = SGL_POLYGON;

    Render();
    vertexBuffer.clear();

    drawingElement = bk;
}

void Context::DrawArc(float x, float y, float z, float radius, float from, float to) {
    sglEElementType bk = drawingElement;
    
    int numSegments = 40 * abs(to - from) / (2 * M_PI);
    float range = abs(from - to);
    float angleStep = range / (float)numSegments;

    if (drawingMode == SGL_FILL) {
        vertexBuffer.resize(numSegments + 2);
        vertexBuffer[numSegments + 1] = Vec4<float>(x, y, z, 1);
        drawingElement = SGL_POLYGON;
    }
    else {
        vertexBuffer.resize(numSegments + 1);
        drawingElement = SGL_LINE_STRIP;
    }

    float CA = cosf(angleStep);
    float SA = sinf(angleStep);
    float x1 = radius * cosf(from);
    float y1 = radius * sinf(from);

    vertexBuffer[0] = Vec4<float>(x + x1, y + y1, z, 1);
    for (int i = 0; i < numSegments; i++) {
        float x2 = CA * x1 - SA * y1;
        float y2 = SA * x1 + CA * y1;
        Vec4<float> v2(x + x2, y + y2, z, 1);
        vertexBuffer[i+1] = v2;
        x1 = x2;
        y1 = y2;
    }
    
    Render();
    vertexBuffer.clear();

    drawingElement = bk;
}

// Draw into color buffer on coordinates [x,y]
void Context::PutPixel(int x, int y) {
    if (x >= 0 && x < width && y >= 0 && y < height) {
        colorBuffer[y * width + x] = color;
    }
}

// Draw 8 symmetrical points around [x0,y0]
void Context::SetSymPixel(int x0, int y0, float z0, int x, int y) {
    int sp[][3]{
        {x0 + x, y0 + y},
        {x0 - x, y0 + y},
        {x0 - y, y0 + x},
        {x0 + y, y0 + x},
        {x0 + x, y0 - y},
        {x0 - x, y0 - y},
        {x0 + y, y0 - x},
        {x0 - y, y0 - x}
    };
    if (!deptTest) {
        if (drawingMode == SGL_LINE) {
            for (auto it : sp) {
                PutPixel(it[0], it[1]);
            }
        }
        else if (drawingMode == SGL_FILL) {
            for (int i = 0; i < 8; i += 2) {
                DrawLine(sp[i][0], sp[i][1], sp[i + 1][0], sp[i + 1][1]);
            }
        }
    }
    else {
        if (drawingMode == SGL_LINE) {
            for (auto it : sp) {
                if (depthBuffer[it[1] * width + it[0]] <= z0) {
                    depthBuffer[it[1] * width + it[0]] = z0;
                    PutPixel(it[0], it[1]);
                }
            }
        }
        else if (drawingMode == SGL_FILL) {
            for (int i = 0; i < 8; i += 2) {
                DrawLineDepth(sp[i][0], sp[i][1], z0, sp[i + 1][0], sp[i + 1][1], z0);
            }
        }
    }
}

/*
void Context::UpdateVPPMV() {
    VPPMVmat = viewMat * transformStack[SGL_PROJECTION].top() * transformStack[SGL_MODELVIEW].top();
}
*/

void Context::UpdatePVM() {
    PVMmat = transformStack[SGL_PROJECTION].top() * transformStack[SGL_MODELVIEW].top();
}

void Context::UpdateVPPVM() {
    UpdatePVM();
    VPPVMmat = viewMat * PVMmat;
}

 void Context::RecalculateEdge(Edge & e, int y) {
    // only if slope of edge is non zero (vertical) add it to x coord
     if (e.slope != 0)
         e.x += e.slope;

    e.x = max(0.0f, min(e.x, (float)width - 1));
}

// calculate a depth value between depth1 and depth2 based on currVal � [val1, val2]
// for correct perspective interpolation, divide depth values as 1/depth before sending them to this fnc
float Context::CalcDepth(int val1, float depth1, int val2, float depth2, int currVal) {
    float alpha = ((float)currVal - (float)val1) / ((float)val2 - (float)val1);
    float z = ((1 - alpha) * depth1) + (alpha * depth2);
    return z;
}


//https://hackernoon.com/computer-graphics-scan-line-polygon-fill-algorithm-3cb47283df6
// filling algorithm
void Context::Fill() {
    // currently actvie edges
    vector<Edge> activeList;
    // currently filled y coordinate
    int currentY = edgesTable.edges[0].yMin;

    // continue until active list and edgestable is empty
    while (!activeList.empty() || !edgesTable.Empty()) {
        // Remove from activeList edges that are finished
        for (vector<Edge>::iterator it = activeList.begin(); it != activeList.end();) {
            if (it->yMax == currentY)
                it = activeList.erase(it);
            else
                it++;
        }

        // Find new edges which starts at currentY and add them to active list
        while (!edgesTable.Empty() && edgesTable.edges[0].yMin == currentY) {
            activeList.push_back(edgesTable.extractMin());
        }

        // Sort all edges by X coord
        sort(activeList.begin(), activeList.end(), [](const Edge& e1, const Edge& e2) {return (e1.x < e2.x); });

        // Draw and recalculate, advance step 2, always take two edges and fill space between them
        for (vector<Edge>::iterator it = activeList.begin(); it != activeList.end(); advance(it, 2)) {
            //printf("filling from %d to %d in height %d\n", it->x, next(it, 1)->x, currentY);
            // second edge
            vector<Edge>::iterator it2 = next(it, 1);
            // if depth test is on
            if (deptTest) {
                // calculate z coordinates for both edges
                float z1 = CalcDepth(it->yMin, it->zMin, it->yMax, it->zMax, currentY);
                float z2 = CalcDepth(it2->yMin, it2->zMin, it2->yMax, it2->zMax, currentY);
                DrawLineDepth(it->x, currentY, z1, next(it, 1)->x, currentY, z2);
                // fill space
                //for (int i = (int)it->x; i < (int)it2->x; i++) {
                //    // final z, interpolation between startx and final x
                //    float z = 1 / CalcDepth(it->x,z1,it2->x,z2,i);
                //    // draw only if z has correct value
                //    if (z <= depthBuffer[currentY * width + i]) {
                //        depthBuffer[currentY * width + i] = z;
                //        PutPixel(i, currentY);
                //    }
                //}
            }
            else { // draw without depth test
                DrawLine(it->x, currentY, it2->x, currentY);
                // fill from start to end
                //for (int i = (int)it->x; i < (int)it2->x; i++) {
                //    PutPixel(i, currentY);
                //}
            }
            // recalculate both current x of both edges
            RecalculateEdge(*it, currentY);
            RecalculateEdge(*it2, currentY);
        }
        // next y coordinate
        currentY++;
    }

    // clear all data
    edgesTable.edges.clear();
    activeList.clear();
}

void Context::AddSphere(float x, float y, float z, float r) {
    Sphere* s = new Sphere(x, y, z, r);
    s->mat = contextScene.currMaterial;
    contextScene.elements.push_back(s);
    //contextScene.spheres.push_back(Sphere(x, y, z, r));
}

void Context::AddLight(float x, float y, float z, float r, float g, float b) {
    contextScene.lights.push_back(Light(x, y, z, r, g, b));
}

float Context::clamp(float item, float min, float max) {
    return (item < min) ? min : (max < item) ? max : item;
}

void Context::Clamp01(Vec3<float>& item) {
    clamp(item[0], 0.f, 1.f);
    clamp(item[1], 0.f, 1.f);
    clamp(item[2], 0.f, 1.f);
}

Vec4<float> Context::Reflect(const Vec4<float> ray, const Vec4<float> normal) {
    //return (ray * -1) + (normal * (2 * clamp(ray.DotProd(normal), 0.f, 1.f)));
    return Normalize(ray - (normal * ray.DotProd(normal) * 2));
}

Vec4<float> Context::Refract(const Vec4<float>& rayDir, const Vec4<float>& hitNormal, const float matIOR, const float NdotD) {
    Vec4<float> Nrefr = hitNormal, refrDir;
    float gamma, sqrterm;
    float dot = NdotD;

    if (dot < 0.0f) {
        // from outside into the inside of object
        gamma = 1.0f / matIOR;
    }
    else {
        //printf("b ");
        // from the inside to outside of object
        gamma = matIOR;
        dot = -dot;
        Nrefr = Nrefr * (-1);
    }
    sqrterm = 1.0f - gamma * gamma * (1.0f - dot * dot);

    // Check for total internal reflection, do nothing if it applies.
    if (sqrterm > 0.0f) {
        sqrterm = dot * gamma + sqrt(sqrterm);
        refrDir = Nrefr * -sqrterm + rayDir * gamma;
    }
    else {
        // total internal reflection occured
        return Reflect(rayDir, Nrefr);
    }
    return refrDir;
}

RGB Context::PhongLighting(Ray & r, Material& mat, Vec4<float> position, Vec4<float> Normal, Vec4<float> lightPos, Vec3<float> lightColor) {
    //Material mat = obj->mat;
    Vec4<float> n = Normal, l, ref;
    Vec4<float> v = r.dir * (-1);
    Vec3<float> lightCol;
    Vec3<float> spec = Vec3<float>(mat.ks, mat.ks, mat.ks);
    Vec3<float> diffPart, specPart, color;
    float cosA, cosB;
    v.Normalize();
    n.Normalize();
    color = Vec3<float>(0.0f, 0.0f, 0.0f);
    

    l = lightPos - position;
    l.Normalize();
    cosA = clamp(n.DotProd(l), 0.f, 1.f);
    ref = Reflect(l * -1, n);
    //ref = (l * -1) + (n * (2 * clamp(l.DotProd(n), 0.f, 1.f)));
    //ref = Reflect(l, n);
    cosB = clamp(ref.DotProd(v), 0.f, 1.f);
    lightCol = lightColor * mat.kd;

    diffPart = lightCol * max(cosA, 0.0f);
    specPart = spec * pow(cosB, mat.shine);

    color = diffPart * mat.color + specPart;

    //Clamp01(color);
    return RGB(color[0], color[1], color[2]);
}

RGB Context::CelShading(Ray& r, Object* obj, Vec4<float> position, Vec4<float> Normal) {
    Vec4<float> n = Normal, l;
    Vec3<float> color;
    float intensity = 0.0f;
    n.Normalize();

    for (unsigned i = 0; i < contextScene.lights.size(); i++) {
        l = contextScene.lights[i].position - position;
        l.Normalize();
        intensity += l.DotProd(n);
    }

    clamp(intensity, 0.f, 1.f);
    if (intensity > 0.95f) {
        color = obj->mat.color * Vec3<float>(1.0f, 1.0f, 1.0f);
    }
    else if (intensity > 0.45f) {
        color = obj->mat.color * Vec3<float>(0.5f, 0.5f, 0.5f);
    }
    else if (intensity > 0.2f) {
        color = obj->mat.color * Vec3<float>(0.2f, 0.2f, 0.2f);
    }
    else {
        color = obj->mat.color * Vec3<float>(0.01f, 0.01f, 0.01f);
    }
    //Clamp01(color);
    return RGB(color[0], color[1], color[2]);
}

void Context::RayCast(unsigned start, unsigned end, const Mat4<float>& invMVPVP, const Vec4<float>& O_ray) {
    //for every pixel within bound of the scene
    RGB pixelColor;
    for (unsigned i = start; i < end; i++) {
        unsigned x = i % width;
        unsigned y = i / width;

        //if (x == 350 && y == 137)
        //if (x == 350 && y == 105)
            //printf("break\n");
        
        // Safety check
        if ((y * width + x) >= colorBuffer.size())
            break;

        //magic constant +0.5f to find pixel centers
        Vec4<float> D_ray(x + 0.5f, y + 0.5f, -1, 1);
        D_ray = invMVPVP * D_ray;

        //ray in world coordinates
        Ray r;
        r.origin = O_ray;
        r.dir = Normalize(D_ray - O_ray);

        RayResult hit = ShootRay(r);
        if (hit.result)
            colorBuffer[i] = hit.color;

    }
}

void Context::PSS(unsigned start, unsigned end, set<pair<int, int>>& superSampleSet) {
    /* Supersampling:
    for pixel, check it's left and bottom neighbor, compare their difference with certain threshold
    if the difference is too big, save these pixels to supersample pixel array
    */
    // lock for critical section
    unique_lock<mutex> superSamplingSetLock(M_SSS, std::defer_lock);
    // tmp set for thread outut
    set<pair<int, int>> tmpSuperSampleSet;

    //threshold for evaluating pixels as to-be-supersampled
    //fiddle around with this value until the scene looks good but only problematic regions are supersampled
    float threshold = 0.2f;
    
    //for every pixel
    for (unsigned i = start; i < end; i++) {
        unsigned x = i % width;
        unsigned y = i / width;

        //if pixel is in first row or column
        if (x <= 0 || y <= 0) continue;
        //calculate difference between left and bottom pixels against center pixel,
        //if value exceeds threshold, add both vertices to set for supersampling
        RGB left = colorBuffer[(y - 1) * width + x];
        RGB bottom = colorBuffer[y * width + (x - 1)];

        RGB dLeft = colorBuffer[i] - left;
        RGB dBottom = colorBuffer[i] - bottom;

        if (abs(dLeft.Length()) > threshold) {
            tmpSuperSampleSet.insert(make_pair(x, y));
            tmpSuperSampleSet.insert(make_pair(x, y - 1));
        }
        if (abs(dBottom.Length()) > threshold) {
            tmpSuperSampleSet.insert(make_pair(x, y));
            tmpSuperSampleSet.insert(make_pair(x - 1, y));
        }
    }
    //printf("SuperSampleSet: %d\n", superSampleSet.size());
    // Synchonization of merging all sets to one set
    superSamplingSetLock.lock();
    superSampleSet.insert(tmpSuperSampleSet.begin(), tmpSuperSampleSet.end());
    superSamplingSetLock.unlock();
}

void Context::MakeSuperSample(unsigned start, unsigned end, const set<pair<int, int> >& superSampleSet, 
                              const Mat4<float>& invMVPVP, const Vec4<float>& O_ray) {
    /* Supersampling:
    for pixel, check it's left and bottom neighbor, compare their difference with certain threshold
    if the difference is too big, save these pixels to supersample pixel array
    */
    // index denoting where we are in limits start end
    unsigned idx = start;
    // if end is larger then actuall set size, set end to that set size
    if (superSampleSet.size() < end)
        end = superSampleSet.size();
    // iterator pointing to the beginning of a set
    set <pair<int, int> >::const_iterator pix = superSampleSet.begin();
    // iterator moved toward the start of current interval
    advance(pix, start);
    for (; idx < end; pix ++, idx++) {
        SuperSample(pix->first, pix->second, invMVPVP, O_ray);
    }
}

RayResult Context::ShootRay(Ray& r, unsigned depth, bool refracted) {
    RayResult res, rRes, refrRes;
    res.ray = r;
    float t_min = std::numeric_limits<float>::max();
    //Object* hitObject = nullptr;
    int triangleIdx = -1;
    //for every object in the scene
    for (auto obj : contextScene.elements) {
        //test intersection for every object in the scene
        //save shortest positive t parameter
        float t_curr = 0.0f;
        int idx = -1;

        if (obj->Intersection(r, t_curr, idx)) {
            
            //backface culling test
            if (!refracted) {
                Vec4<float> hitPoint = r.origin + (r.dir * t_curr);
                Vec4<float> hitNormal = obj->GetNormal(hitPoint, idx);
                //printf("[%f, %f, %f]\n", hitNormal[0], hitNormal[1], hitNormal[2]);
                if (hitNormal.DotProd(r.dir) > 0.0f) continue;
            }
            
            if (t_curr < 0.1f) continue;
            
            if (t_curr > 0.0f && t_curr < t_min) {
                t_min = t_curr;
                res.object = obj;
                triangleIdx = idx; //save index of hit triangle
            }
        }
    }

    if (t_min == std::numeric_limits<float>::max()) {
        //ray didnt hit any object in the scene
        res.result = false;
        if (contextScene.bgEnv.width != -1) {
            res.result = true;
            res.color = contextScene.GetBGEnv(r);
        }
        return res;
    }

    // Calculate the hit point
    res.point = r.origin + (r.dir * t_min);
    // Calculate normal of the hitted object
    res.normal = res.object->GetNormal(res.point, triangleIdx);

    //ray hit area light
    if (res.object->isEmissive) {
        RGB col(res.object->eMat.color[0], res.object->eMat.color[1], res.object->eMat.color[2]);
        res.color = col;
        return res;
    }

    // point light
    for (unsigned i = 0; i < contextScene.lights.size(); i++) {
        // If hit object isn't occluded, compute phong
        if (!IsOccluded(res, contextScene.lights[i].position))
            res.color = res.color + PhongLighting(r, res.object->mat, res.point, res.normal, 
                contextScene.lights[i].position, contextScene.lights[i].color);
    }
    // area light
    for (unsigned int i = 0; i < contextScene.areaLights.size(); i++) {
        TriangleMesh* lightTri;
        if (lightTri = dynamic_cast<TriangleMesh*>(contextScene.areaLights[i])) {
            for (unsigned int j = 0; j < lightTri->triangles.size(); j++) {
                res.color = res.color + ComputeAreaLight(res, lightTri->triangles[j], contextScene.areaLights[i]->eMat);
            }
        }
    }

    //reflected ray
    if (res.object->mat.ks > 0.0f && depth < MAXDEPTH) {
        // Origin of reflection
        r.origin = res.point + res.normal * 0.1f;
        // Compute new direction
        r.dir = Reflect(r.dir,res.normal);
        // Shoot new ray
        rRes = ShootRay(r, depth + 1);
        // If any object was hit then compute color
        if (rRes.result) {
            rRes.color = rRes.color *res.object->mat.ks;
        }
    }

    //refracted ray
    if (res.object && res.object->mat.T > 0.0f && depth < MAXDEPTH) {
        // Compute constant which can change where to shift refracted ray origin
        float k1 = r.dir.DotProd(res.normal);
        // Decide where to shift (inside/outside)
        if (k1 < 0)
            r.origin = res.point - res.normal * 0.01f;
        else
            r.origin = res.point + res.normal * 0.01f;
        // Compute refraction
        r.dir = Refract(r.dir, res.normal, res.object->mat.ior, k1);
        // Shoot new ray
        refrRes = ShootRay(r, depth + 1, true);
        // Compute new color if anything was hit
        if (refrRes.result) {
            refrRes.color = refrRes.color *res.object->mat.T;
        }
    }

    // add colors from refracted and reflected components
    res.color = res.color + rRes.color + refrRes.color;

    // clamps color to values [0,1]
    //res.color.Clamp(); // <- Commented out because it messed with env mapping
    
    return res;
}

RGB Context::ComputeAreaLight(RayResult& hit, Triangle& tri, EmissiveMaterial& mat) {
    std::vector<Vec4<float>> samplePoints;
    int sampleCount = 16;

    Ray ray;
    ray.origin = hit.point + hit.normal * 0.1f;

    Vec4<float> e1 = tri.vertices[1] - tri.vertices[0];
    Vec4<float> e2 = tri.vertices[2] - tri.vertices[0];
    Vec4<float> e12cross = e1.CrossProd(e2);
    float triArea = 0.5 * e12cross.Lenght();
    float triAreaNorm = triArea / sampleCount;
    Vec4<float> triNormal = e12cross/e12cross.Lenght();
    RGB finalColor(0.0f, 0.0f, 0.0f);
    //generate sample points on light
    for (int i = 0; i < sampleCount; i++) {
        float r1 = GenRandomFloat();
        float r2 = GenRandomFloat();
        float u, v;
        if (r1 + r2 > 1) {
            u = 1 - r1;
            v = 1 - r2;
        }
        else {
            u = r1;
            v = r2;
        }
        Vec4<float> pos = tri.vertices[0] + (e1 * u) + (e2 * v);
        samplePoints.push_back(pos);

        ray.dir = pos - ray.origin;
        float dist = ray.dir.Lenght() - 0.05f;
        ray.dir.Normalize();
        if (!IsOccluded(ray, dist)) {
            float fi = triNormal.DotProd(ray.dir * -1);
            float weight = fi * triAreaNorm / (mat.attenuation[0] + dist * mat.attenuation[1] + powf(dist, 2) * mat.attenuation[2]);
            RGB color = PhongLighting(hit.ray, hit.object->mat, hit.point, hit.normal, pos, mat.color) * weight;
                //PhongLighting(r, res.object, res.point, res.normal, 
                //contextScene.lights[i].position, contextScene.lights[i].color);
            finalColor = finalColor + color;
        }
        else {
            int x = 10;
        }
    }

    return finalColor;
}

float Context::GenRandomFloat() {
    static std::default_random_engine eng;
    static std::uniform_real_distribution<> distr(0, 1);
    return distr(eng);
}

void Context::SuperSample(int x, int y, const Mat4<float>& invMVPVP, const Vec4<float>& O_ray) {
    //sampling frequency
    float step = 0.33333f;

    //starting color = current center of pixel
    RGB finalColor = colorBuffer[y * width + x];

    //calculate subpixel points (one pixel = 9 points)
    for (float i = x + 0.5f - step; i <= x + 0.5f + step; i += step) {
        for (float j = y + 0.5f - step; j <= y + 0.5f + step; j += step) {
            if ((i == (x + 0.5f)) && (j == (y + 0.5f))) continue; //skip the middle
            Vec4<float> D_ray(i, j, -1, 1);
            D_ray = invMVPVP * D_ray;

            //ray in world coordinates
            Ray r;
            r.origin = O_ray;
            r.dir = Normalize(D_ray - O_ray);

            RayResult hit = ShootRay(r);
            if (!hit.result) {
                finalColor = finalColor + clearColor;
                continue; //ray didn't hit anything
            }

#ifndef CELSHADING // define is located in ContextBuffers.h on the top of the file before context class definition
            for (unsigned i = 0; i < contextScene.lights.size(); i++) {
                finalColor = finalColor + PhongLighting(r, hit.object->mat, hit.point, hit.normal, 
                    contextScene.lights[i].position, contextScene.lights[i].color);
            }
#else // !CELSHADING
            finalColor = finalColor + CelShading(r, hit.object, hit.point, hit.normal);
#endif

        }
    }
    colorBuffer[y * width + x] = finalColor / 9.0f; //normalize (average of 9 subpixel calculated values)
}

bool Context::IsOccluded(const RayResult& hit, Vec4<float>& lightPos) {
    // Shadow ray
    Ray ray;
    // Check constants
    float t_curr = 0.f, distance = 0.f;
    // idx of hit object
    int idx = 0;
    // Shifted hit point as origin of the shadow ray
    ray.origin = hit.point + hit.normal * 0.1f;

    // Compute new direction
    ray.dir = lightPos - ray.origin;
    // New distance with small shift
    distance = ray.dir.Lenght() - 0.05f;
    ray.dir.Normalize();
    // Check the shadow ray against all objects
    for (Object* obj : contextScene.elements) {
        if (obj->Intersection(ray, t_curr, idx) && t_curr <= distance) {
            //play around with this value if shadows look wrong
            if (t_curr < 0.1f) continue;
            // Occluded
            return true;
        }
    }

    // Not occluded
    return false;
}

bool Context::IsOccluded(Ray& ray, float distance) {
    // Check constants
    float t_curr = 0.f;
    // idx of hit object
    int idx = 0;
    // New distance with small shift
    //distance = ray.dir.Lenght() - 0.05f;
    // Check the shadow ray against all objects
    for (Object* obj : contextScene.elements) {
        if (obj->Intersection(ray, t_curr, idx) && t_curr <= distance) {
            //play around with this value if shadows look wrong
            if (t_curr < 0.003f) continue;
            // Occluded
            return true;
        }
    }
    // Not occluded
    return false;
}