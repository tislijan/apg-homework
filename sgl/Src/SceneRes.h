#pragma once
#include <vector>
#include <limits>
#include <cstdio>
#include <string>
#include "MatrixVector.h"

// Color buffer struct, holds only information about color of a pixel
struct RGB {
	// Red channel
	float r;
	// Green channel
	float g;
	// Blue channel
	float b;

	// Nonparametric constructor of RGB struct
	RGB() : r(0), g(0), b(0) {}
	// Parametric constructor of RGB struct
	RGB(float rr, float gg, float bb) : r(rr), g(gg), b(bb) {}
	// Compare operator of RGB struct
	bool operator==(RGB other) {
		return (r == other.r && g == other.g && b == other.b);
	}
	// Overloaded operators of RGB class
	RGB operator+ (const RGB& rhs) const;
	RGB operator- (const RGB& rhs) const;
	RGB operator/ (const float rhs) const;
	RGB operator* (const float rhs) const;

	// Returns length of color vector
	float Length();
	// Normalizes color of RGB element
	void Normalize();
	// Clamps the values to interval [0,1]
	void Clamp();
};

struct EnvMap
{
	// Width of a map, -1 if there is none env map
	int width = -1;
	// Height of a map, -1 if there is none
	int height = -1;
	// Size of the env map
	int size = 0;
	// Vector of all texels if any
	std::vector<float> texels;
};

// Class representing one material
class Material {
public:
	// Color assigned to a material
	Vec3<float> color;
	// Diffuse coeficient of material
	float kd;
	// Specular coeficient of material
	float ks;
	// Shinnines of material
	float shine;
	// the transmittance coefficient (fraction of contribution of the transmitting / refracted ray)
	float T;
	// Index of refraction
	float ior;
};

// Class describing emmisive material
class EmissiveMaterial {
public:
	// Color of emission
	Vec3<float> color;
	// Atteuantion factor of the material
	Vec3<float> attenuation;
};

// Light representation
class Light {
public:
	// Lights position
	Vec4<float> position;
	// Assigned color
	Vec3<float> color;

	// Parametric constructor
	Light(float x, float y, float z, float rr, float gg, float bb) {
		position[0] = x;
		position[1] = y;
		position[2] = z;
		color = Vec3<float>(rr, gg,bb);
	}
};

// Ray informations
class Ray {
public:
	// Origin of light
	Vec4<float> origin;
	// Direction of light
	Vec4<float> dir;
	// Parametric constructor
	Ray() : origin(Vec4<float>(0.0f)), dir(Vec4<float>(0.0f)) {}
	//float mint;
	//float maxt;
};

// Object class representing all objects
class Object {
public:
	// Material of assigned object(s)
	Material mat;
	bool isEmissive;
	EmissiveMaterial eMat;
	// Information output about an object
	virtual void PrintInfo() = 0;
	// Compute normal of an object
	virtual Vec4<float> GetNormal(Vec4<float>& intersectionP, int triangleIdx) = 0;
	// Compute if a ray intersected with any object
	virtual bool Intersection(const Ray& ray, float& t, int &idx) = 0;

	virtual ~Object() = default;
};

// Sphere class derivated from object class
class Sphere : public Object {
public:
	// Spheres center position
	Vec4<float> position;
	// Spheres radius
	float radius;

	// Parametric consturctor of sphere calss
	Sphere(float x, float y, float z, float r) {	
		position[0] = x;
		position[1] = y;
		position[2] = z;
		radius = r;
		isEmissive = false;
	}
	// sphere Intersection() - apg cw.feld..
	bool Intersection(const Ray& ray, float& t, int &idx);
	// Sphere normal calculation
	Vec4<float> GetNormal(Vec4<float>& intersectionP, int triangleIdx);
	// Print spheres information
	void PrintInfo();
};

// Triangle representing class, derivated from object class
class Triangle {
public:
	// Triangles points in space
	Vec4<float> vertices[3];

	// Parametric constructor
	Triangle(Vec4<float> v0, Vec4<float> v1, Vec4<float> v2);
	// triangle Intersection() - apg cw.feld..
	bool Intersection(const Ray& ray, float& t);
};

// Triangle mesh, again derivated from object
class TriangleMesh : public Object {
public:
	// All triangles of one triangular mesh
	std::vector<Triangle> triangles;
	// Triangle intersection function
	bool Intersection(const Ray& ray, float& t, int &idx);
	// Normal of a triangle
	Vec4<float> GetNormal(Vec4<float>& intersectionP, int triangleIdx);
	// Print trianglemesh info
	void PrintInfo();

	TriangleMesh() {
		isEmissive = false;
	}
};

// Scene with all objects, lights, ...
class Scene {
public:
	// All elements of a scene (triangular meshs and spheres, including area lights)
	std::vector<Object*> elements;
	// All lights
	std::vector<Light> lights;
	// Pointers to all area lights (also included in elements array)
	std::vector<Object*> areaLights;

	// Current material for incoming geometry
	Material currMaterial;
	// Current emissive material for incoming geometry
	EmissiveMaterial currEmissiveMaterial;
	bool useEmissive;
	// Triangular mesh, which is not yet finished
	TriangleMesh* currObj;
	// Background environment map
	EnvMap bgEnv;

	// Aparametric scene constructor;
	Scene() {
		currObj = nullptr;
		useEmissive = false;
	}
	// Set Env map for a scene
	void SetBGEnv(const int w, const int h, float* texs);
	// Computes color on background, env maps
	RGB GetBGEnv( const Ray & ray );
};
