workspace "Apg"
	architecture "x64"
	startproject "testApp"
	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["glut"] = "Apg/freeglut/include/GL"

project "testApp"
	location "testApp"
	kind "ConsoleApp"
	staticruntime "On"
	
	language "C++"
	cppdialect "C++Latest"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	
	defines{
		"USE_GUI=1",
		"TEST0"
	}

	files
	{
	"%{prj.name}/Src/hdrloader.cpp",
	"%{prj.name}/Src/nffread.cpp",
	"%{prj.name}/Src/testapp.cpp",
	"%{prj.name}/Src/timer.cpp",
	"%{prj.name}/hdrloader.h",
	"%{prj.name}/nffread.h",
	"%{prj.name}/nffstore.h",
	"%{prj.name}/nffwrite.h",
	"%{prj.name}/timer.h"
	}

	includedirs
	{
	"sgl/Src",
	"%{prj.name}/Src",
	"%{prj.name}/freeglut/include",
	"%{IncludeDir.glut}"
	}
	
	links
	{
		"freeglut",
		"sgl"
	}

	filter "system:windows"
		systemversion "latest"

		defines{
			"D__WINDOWS__", 
			"D_CRT_SECURE_NO_WARNINGS",
			"D_CRT_SECURE_NO_DEPRECATE",
		}

		
	filter "configurations:Debug"
		buildoptions "/MDd"
		symbols "on"

	filter "configurations:Release"
		buildoptions "/MD"
		optimize "on"

	filter "configurations:Dist"
		buildoptions "/MD"
		optimize "on"

project "sgl"
	location "sgl"
	kind "StaticLib"
	staticruntime "On"
	
	language "C++"
	cppdialect "C++Latest"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
	
	files
	{
	"%{prj.name}/Src/**.h",
	"%{prj.name}/Src/**.cpp"
	}


	filter "system:windows"
		systemversion "latest"


	filter "configurations:Debug"
		buildoptions "/MDd"
		symbols "On"

	filter "configurations:Release"
		buildoptions "/MD"
		optimize "On"

	filter "configurations:Dist"
		buildoptions "/MD"
		optimize "On"